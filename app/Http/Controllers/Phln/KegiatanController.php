<?php

namespace App\Http\Controllers\Phln;

use Carbon\Carbon;
use App\Models\Rate;
use App\Models\Unor;
use App\Models\Donor;
use App\Models\Satker;
use App\Models\Kegiatan;
use App\Models\MataUang;
use App\Models\PaketAwp;
use App\Models\Department;
use App\Models\Penyerapan;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class KegiatanController extends Controller
{
    public function index(Request $request)
    {
        $sektor_user = Auth::guard('office')->user()->sektor;
        if($sektor_user == 5){
            $ar = Kegiatan::where('tipe_kegiatan','=','Pinjaman')
            ->where('st','=','At Risk')
            ->orderBy('id', 'ASC')
            ->get()->count();
            $bs = Kegiatan::where('tipe_kegiatan','=','Pinjaman')
            ->where('st','=','Behind Schedule')
            ->orderBy('id', 'ASC')
            ->get()->count();
            $os = Kegiatan::where('tipe_kegiatan','=','Pinjaman')
            ->where('st','=','On Schedule')
            ->orderBy('id', 'ASC')
            ->get()->count();
        }else{
            $ar = Kegiatan::where('tipe_kegiatan','=','Pinjaman')
            ->where('sektor','=',$sektor_user)
            ->where('st','=','At Risk')
            ->orderBy('id', 'ASC')
            ->get()->count();
            $bs = Kegiatan::where('tipe_kegiatan','=','Pinjaman')
            ->where('sektor','=',$sektor_user)
            ->where('st','=','Behind Schedule')
            ->orderBy('id', 'ASC')
            ->get()->count();
            $os = Kegiatan::where('tipe_kegiatan','=','Pinjaman')
            ->where('sektor','=',$sektor_user)
            ->where('st','=','On Schedule')
            ->orderBy('id', 'ASC')
            ->get()->count();
        }
        if ($request->ajax()) {
            $keywords = strtoupper($request->keyword);
            if($sektor_user == 5){
                $collection = Kegiatan::where('tipe_kegiatan','=','Pinjaman')
                ->where('kode_register','LIKE','%'.$keywords.'%')
                ->whereRaw('UPPER("judul") LIKE \'%'.$keywords.'%\'')
                ->orderBy('id', 'ASC')
                ->paginate(10);
            }else{
                $collection = Kegiatan::where('tipe_kegiatan','=','Pinjaman')
                ->where('sektor','=',$sektor_user)
                ->where('kode_register','LIKE','%'.$keywords.'%')
                ->whereRaw('UPPER("judul") LIKE \'%'.$keywords.'%\'')
                ->orderBy('id', 'ASC')
                ->paginate(10);
            }
            return view('page.app.kegiatan.list',compact('collection'));
        }
        return view('page.app.kegiatan.main',compact('ar','bs','os'));
    }
    public function risk()
    {
        $sektor_user = Auth::guard('office')->user()->sektor;
        if($sektor_user == 5){
            $collection = Kegiatan::where('tipe_kegiatan','=','Pinjaman')
            ->where('st','=','At Risk')
            ->orderBy('id', 'ASC')
            ->get();
            $bs = Kegiatan::where('tipe_kegiatan','=','Pinjaman')
            ->where('st','=','Behind Schedule')
            ->orderBy('id', 'ASC')
            ->get()->count();
            $os = Kegiatan::where('tipe_kegiatan','=','Pinjaman')
            ->where('st','=','On Schedule')
            ->orderBy('id', 'ASC')
            ->get()->count();
        }else{
            $collection = Kegiatan::where('tipe_kegiatan','=','Pinjaman')
            ->where('sektor','=',$sektor_user)
            ->where('st','=','At Risk')
            ->orderBy('id', 'ASC')
            ->get();
            $bs = Kegiatan::where('tipe_kegiatan','=','Pinjaman')
            ->where('sektor','=',$sektor_user)
            ->where('st','=','Behind Schedule')
            ->orderBy('id', 'ASC')
            ->get()->count();
            $os = Kegiatan::where('tipe_kegiatan','=','Pinjaman')
            ->where('sektor','=',$sektor_user)
            ->where('st','=','On Schedule')
            ->orderBy('id', 'ASC')
            ->get()->count();
        }
        return view('page.app.kegiatan.risk',compact('collection','bs','os'));
    }
    public function bs()
    {
        $sektor_user = Auth::guard('office')->user()->sektor;
        if($sektor_user == 5){
            $collection = Kegiatan::where('tipe_kegiatan','=','Pinjaman')
            ->where('st','=','Behind Schedule')
            ->orderBy('id', 'ASC')
            ->get();
            $ar = Kegiatan::where('tipe_kegiatan','=','Pinjaman')
            ->where('st','=','At Risk')
            ->orderBy('id', 'ASC')
            ->get()->count();
            $os = Kegiatan::where('tipe_kegiatan','=','Pinjaman')
            ->where('st','=','On Schedule')
            ->orderBy('id', 'ASC')
            ->get()->count();
            return view('page.app.kegiatan.bs',compact('collection','ar','os'));
        }else{
            $collection = Kegiatan::where('tipe_kegiatan','=','Pinjaman')
            ->where('sektor','=',$sektor_user)
            ->where('st','=','Behind Schedule')
            ->orderBy('id', 'ASC')
            ->get();
            $ar = Kegiatan::where('tipe_kegiatan','=','Pinjaman')
            ->where('sektor','=',$sektor_user)
            ->where('st','=','At Risk')
            ->orderBy('id', 'ASC')
            ->get()->count();
            $os = Kegiatan::where('tipe_kegiatan','=','Pinjaman')
            ->where('sektor','=',$sektor_user)
            ->where('st','=','On Schedule')
            ->orderBy('id', 'ASC')
            ->get()->count();
            return view('page.app.kegiatan.bs',compact('collection','ar','os'));
        }
    }
    public function os()
    {
        $sektor_user = Auth::guard('office')->user()->sektor;
        if($sektor_user == 5){
            $collection = Kegiatan::where('tipe_kegiatan','=','Pinjaman')
            ->where('st','=','On Schedule')
            ->orderBy('id', 'ASC')
            ->get();
            $ar = Kegiatan::where('tipe_kegiatan','=','Pinjaman')
            ->where('st','=','At Risk')
            ->orderBy('id', 'ASC')
            ->get()->count();
            $bs = Kegiatan::where('tipe_kegiatan','=','Pinjaman')
            ->where('st','=','Behind Schedule')
            ->orderBy('id', 'ASC')
            ->get()->count();
        }else{
            $collection = Kegiatan::where('tipe_kegiatan','=','Pinjaman')
            ->where('sektor','=',$sektor_user)
            ->where('st','=','On Schedule')
            ->orderBy('id', 'ASC')
            ->get();
            $ar = Kegiatan::where('tipe_kegiatan','=','Pinjaman')
            ->where('sektor','=',$sektor_user)
            ->where('st','=','At Risk')
            ->orderBy('id', 'ASC')
            ->get()->count();
            $bs = Kegiatan::where('tipe_kegiatan','=','Pinjaman')
            ->where('sektor','=',$sektor_user)
            ->where('st','=','Behind Schedule')
            ->orderBy('id', 'ASC')
            ->get()->count();
            return view('page.app.kegiatan.os',compact('collection','ar','bs'));
        }
    }
    public function create()
    {
        $donor = Donor::get();
        $mata_uang = MataUang::get();
        $department = Department::get();
        $unor = Unor::get();
        $satker = Satker::get();
        return view('page.app.kegiatan.input', ['data' => new Kegiatan, 'donor' => $donor, 'mata_uang' => $mata_uang , 'department' => $department, 'unor' => $unor, 'satker' => $satker]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'kode_register' => 'required|unique:pgsql.transaction.kegiatan',
            'no_loan' => 'required',
            'donor' => 'required',
            'mata_uang' => 'required',
            'judul' => 'required',
            'tujuan' => 'required',
            'nilai' => 'required',
            'sasaran' => 'required',
            'komponen' => 'required',
            'lingkeg' => 'required',
            'tanggal_efektif' => 'required|date_format:d-m-Y',
            'tanggal_closing' => 'required|date_format:d-m-Y|after:tanggal_efektif',
                // 'exec_kl' => 'required',
                // 'exec_unor' => 'required',
                // 'exec_satker' => 'required',
                // 'imp_kl' => 'required',
                // 'imp_unor' => 'required',
                // 'imp_satker' => 'required',
                // 'dipa_pagu' => 'required',
                // 'dipa_anggaran' => 'required',
                // 'kinerja_serapan' => 'required',
                // 'jumlah_penarikan' => 'required',
                // 'ta' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('kode_register')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('kode_register'),
                ]);
            }elseif($errors->has('no_loan')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('no_loan'),
                ]);
            }elseif($errors->has('donor')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('donor'),
                ]);
            }elseif($errors->has('mata_uang')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('mata_uang'),
                ]);
            }elseif($errors->has('judul')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('judul'),
                ]);
            }elseif($errors->has('tujuan')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('tujuan'),
                ]);
            }elseif($errors->has('nilai')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nilai'),
                ]);
            }elseif($errors->has('tanggal_efektif')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('tanggal_efektif'),
                ]);
            }elseif($errors->has('tanggal_closing')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('tanggal_closing'),
                ]);
            }elseif($errors->has('sasaran')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('sasaran'),
                ]);
            }elseif($errors->has('komponen')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('komponen'),
                ]);
            }elseif($errors->has('lingkeg')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('lingkeg'),
                ]);
            }
            // elseif($errors->has('exec_kl')){
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('exec_kl'),
            //     ]);
            // }elseif($errors->has('exec_unor')){
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('exec_unor'),
            //     ]);
            // }elseif($errors->has('exec_satker')){
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('exec_satker'),
            //     ]);
            // }elseif($errors->has('imp_kl')){
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('imp_kl'),
            //     ]);
            // }elseif($errors->has('imp_unor')){
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('imp_unor'),
            //     ]);
            // }elseif($errors->has('imp_satker')){
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('imp_satker'),
            //     ]);
            // }
            // elseif($errors->has('dipa_pagu')){
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('dipa_pagu'),
            //     ]);
            // }elseif($errors->has('dipa_anggaran')){
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('dipa_anggaran'),
            //     ]);
            // }elseif($errors->has('kinerja_serapan')){
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('kinerja_serapan'),
            //     ]);
            // }elseif($errors->has('jumlah_penarikan')){
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('jumlah_penarikan'),
            //     ]);
            // }elseif($errors->has('ta')){
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('ta'),
            //     ]);
            // }
        }
        $data = new Kegiatan;
        $data->kode_register = $request->kode_register;
        $data->no_loan = $request->no_loan;
        $data->donor_id = $request->donor;
        $data->mata_uang_id = $request->mata_uang;
        $data->judul = Str::title($request->judul);
        $data->tujuan = $request->tujuan;
        $data->nilai = str_replace(',','',$request->nilai);
        $data->tanggal_efektif = $request->tanggal_efektif;
        $data->tanggal_closing = $request->tanggal_closing;
        $data->exec_kl_id = 0;
        $data->exec_unor_id = 0;
        $data->exec_satker_id = 0;
        $data->imp_kl_id = 0;
        $data->imp_unor_id = 0;
        $data->imp_satker_id = 0;
        $data->exec_kl_code = $request->exec_kl ?? 0;
        $data->exec_unor_code = $request->exec_unor ?? 0;
        $data->exec_satker_code = $request->exec_satker ?? 0;
        $data->imp_kl_code = $request->imp_kl ?? 0;
        $data->imp_unor_code = $request->imp_unor ?? 0;
        $data->imp_satker_code = $request->imp_satker ?? 0;
        $data->sasaran = $request->sasaran;
        $data->komponen = $request->komponen;
        $data->lingkup_kegiatan = $request->lingkeg;
        $data->sektor = $request->sektor;
        $data->metode_pembayaran = $request->metode_pembayaran;
        // $data->dipa_pagu = str_replace(',','',$request->dipa_pagu);
        // $data->dipa_anggaran = str_replace(',','',$request->dipa_anggaran);
        // $data->kinerja_serapan = str_replace(',','',$request->kinerja_serapan);
        // $data->jumlah_penarikan = str_replace(',','',$request->jumlah_penarikan);
        // $data->ta = $request->ta;
        // $today = date('Y-m-d');
        // $efektif = $request->tanggal_efektif;
        // $closing = $request->tanggal_closing;
        // $etr1 = $efektif->diffInDays($today);
        // $etr2 = $closing->diffInDays($efektif);
        // $etr = $etr1/$etr2;
        $data->etr = 0;
        $data->dr = 0;
        $data->pv = 0;

        $mata_uang_kegiatan = MataUang::where('id',$request->mata_uang)->first();
        if($mata_uang_kegiatan->kode != "IDR"){
            $rate_kegiatan = Rate::where('awal' ,'<=' ,date('Y-m-d'))
            ->where('akhir' ,'>=' ,date('Y-m-d'))
            ->where('mata_uang_id' ,'=' ,$mata_uang_kegiatan->id)
            ->first();
            $nilai_kurs_kegiatan = $rate_kegiatan->rate;
            $nilai_konversi = floatval($request->nilai)*floatval($nilai_kurs_kegiatan);
        }else{
            $nilai_konversi = $request->nilai;
        }
        $data->nilai_konversi = $nilai_konversi;
        $today = date('Y-m-d');
        $efektif = Carbon::parse($request->tanggal_efektif);
        $closing = Carbon::parse($request->tanggal_closing);
        // $efektif = $request->tanggal_efektif;
        // $closing = $request->tanggal_closing;
        $etr1 = $efektif->diffInDays($today);
        $etr2 = $closing->diffInDays($efektif);
        $etr = $etr1/$etr2;
        $dr = 0;
        // $pv = $dr/$etr;
        $pv = 0;
        $st = '';
        if($dr = 0){
            if($etr > 0.7){
                $st = 'At Risk';
            }elseif($etr <= 0.7){
                $st = 'Behind Schedule';
            }
        }else{
            if($pv <= 0.3 || $etr > 0.7){
                $st = 'At Risk';
            }elseif($pv >= 0.3 AND $pv <= 1 || $etr <= 0.7){
                $st = 'Behind Schedule';
            }elseif($pv >= 1){
                $st = 'On Schedule';
            }
        }
        $data->st = $st;
        $data->tipe_kegiatan = "Pinjaman";
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Kegiatan tersimpan',
            'redirect' => 'input',
            'route' => route('phln.kegiatan.edit',$data->id),
        ]);
    }
    public function show(Kegiatan $kegiatan)
    {
        //
    }
    public function edit(Kegiatan $kegiatan)
    {
        $donor = Donor::get();
        $mata_uang = MataUang::get();
        $department = Department::get();
        $unor = Unor::get();
        $satker = Satker::get();
        return view('page.app.kegiatan.input', ['data' => $kegiatan, 'donor' => $donor, 'mata_uang' => $mata_uang , 'department' => $department, 'unor' => $unor, 'satker' => $satker]);
    }
    public function update(Request $request, Kegiatan $kegiatan)
    {
        $validator = Validator::make($request->all(), [
            'kode_register' => 'required',
            'no_loan' => 'required',
            'donor' => 'required',
            'mata_uang' => 'required',
            'judul' => 'required',
            'tujuan' => 'required',
            'nilai' => 'required',
            'sasaran' => 'required',
            'komponen' => 'required',
            'lingkeg' => 'required',
            'tanggal_efektif' => 'required|date_format:d-m-Y',
            'tanggal_closing' => 'required|date_format:d-m-Y|after:tanggal_efektif',
            // 'exec_kl' => 'required',
            // 'exec_unor' => 'required',
            // 'exec_satker' => 'required',
            // 'imp_kl' => 'required',
            // 'imp_unor' => 'required',
            // 'imp_satker' => 'required',
            // 'dipa_pagu' => 'required',
            // 'dipa_anggaran' => 'required',
            // 'kinerja_serapan' => 'required',
            // 'jumlah_penarikan' => 'required',
            // 'ta' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('kode_register')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('kode_register'),
                ]);
            }elseif($errors->has('no_loan')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('no_loan'),
                ]);
            }elseif($errors->has('donor')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('donor'),
                ]);
            }elseif($errors->has('mata_uang')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('mata_uang'),
                ]);
            }elseif($errors->has('judul')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('judul'),
                ]);
            }elseif($errors->has('tujuan')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('tujuan'),
                ]);
            }elseif($errors->has('nilai')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nilai'),
                ]);
            }elseif($errors->has('tanggal_efektif')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('tanggal_efektif'),
                ]);
            }elseif($errors->has('tanggal_closing')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('tanggal_closing'),
                ]);
            }elseif($errors->has('sasaran')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('sasaran'),
                ]);
            }elseif($errors->has('komponen')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('komponen'),
                ]);
            }elseif($errors->has('lingkeg')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('lingkeg'),
                ]);
            }
            // elseif($errors->has('exec_kl')){
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('exec_kl'),
            //     ]);
            // }elseif($errors->has('exec_unor')){
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('exec_unor'),
            //     ]);
            // }elseif($errors->has('exec_satker')){
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('exec_satker'),
            //     ]);
            // }elseif($errors->has('imp_kl')){
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('imp_kl'),
            //     ]);
            // }elseif($errors->has('imp_unor')){
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('imp_unor'),
            //     ]);
            // }elseif($errors->has('imp_satker')){
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('imp_satker'),
            //     ]);
            // }
            // elseif($errors->has('dipa_pagu')){
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('dipa_pagu'),
            //     ]);
            // }elseif($errors->has('dipa_anggaran')){
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('dipa_anggaran'),
            //     ]);
            // }elseif($errors->has('kinerja_serapan')){
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('kinerja_serapan'),
            //     ]);
            // }elseif($errors->has('jumlah_penarikan')){
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('jumlah_penarikan'),
            //     ]);
            // }elseif($errors->has('ta')){
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('ta'),
            //     ]);
            // }
        }
        $kegiatan->kode_register = $request->kode_register;
        $kegiatan->no_loan = $request->no_loan;
        $kegiatan->donor_id = $request->donor;
        $kegiatan->mata_uang_id = $request->mata_uang;
        $kegiatan->judul = Str::title($request->judul);
        $kegiatan->tujuan = $request->tujuan;
        $kegiatan->nilai = str_replace(',','',$request->nilai);
        $kegiatan->tanggal_efektif = $request->tanggal_efektif;
        $kegiatan->tanggal_closing = $request->tanggal_closing;
        $kegiatan->exec_kl_id = 0;
        $kegiatan->exec_unor_id = 0;
        $kegiatan->exec_satker_id = 0;
        $kegiatan->imp_kl_id = 0;
        $kegiatan->imp_unor_id = 0;
        $kegiatan->imp_satker_id = 0;
        $kegiatan->exec_kl_code = $request->exec_kl ?? 0;
        $kegiatan->exec_unor_code = $request->exec_unor ?? 0;
        $kegiatan->exec_satker_code = $request->exec_satker ?? 0;
        $kegiatan->imp_kl_code = $request->imp_kl ?? 0;
        $kegiatan->imp_unor_code = $request->imp_unor ?? 0;
        $kegiatan->imp_satker_code = $request->imp_satker ?? 0;
        $kegiatan->sasaran = $request->sasaran;
        $kegiatan->komponen = $request->komponen;
        $kegiatan->sektor = $request->sektor;
        $kegiatan->metode_pembayaran = $request->metode_pembayaran;
        $kegiatan->lingkup_kegiatan = $request->lingkeg;
        $mata_uang_kegiatan = MataUang::where('id',$request->mata_uang)->first();
        if($mata_uang_kegiatan->kode != "IDR"){
            $rate_kegiatan = Rate::where('awal' ,'<=' ,date('Y-m-d'))
            ->where('akhir' ,'>=' ,date('Y-m-d'))
            ->where('mata_uang_id' ,'=' ,$mata_uang_kegiatan->id)
            ->first();
            $nilai_kurs_kegiatan = $rate_kegiatan->rate;
            $nilai_konversi = floatval($request->nilai)*floatval($nilai_kurs_kegiatan);
        }else{
            $nilai_konversi = $request->nilai;
        }
        $kegiatan->nilai_konversi = $nilai_konversi;
        // $kegiatan->dipa_pagu = str_replace(',','',$request->dipa_pagu);
        // $kegiatan->dipa_anggaran = str_replace(',','',$request->dipa_anggaran);
        // $kegiatan->kinerja_serapan = str_replace(',','',$request->kinerja_serapan);
        // $kegiatan->jumlah_penarikan = str_replace(',','',$request->jumlah_penarikan);
        // $kegiatan->ta = $request->ta;
        $kegiatan->tipe_kegiatan = "Pinjaman";
        $today = date('Y-m-d');
        $efektif = Carbon::parse($request->tanggal_efektif);
        $closing = Carbon::parse($request->tanggal_closing);
        $etr1 = $efektif->diffInDays($today);
        $etr2 = $closing->diffInDays($efektif);
        $etr = $etr1/$etr2;
        $dr = 0;
        // $pv = $dr/$etr;
        $pv = 0;
        $st = '';
        if($dr = 0){
            if($etr > 0.7){
                $st = 'At Risk';
            }elseif($etr <= 0.7){
                $st = 'Behind Schedule';
            }
        }else{
            if($pv <= 0.3 || $etr > 0.7){
                $st = 'At Risk';
            }elseif($pv >= 0.3 AND $pv <= 1 || $etr <= 0.7){
                $st = 'Behind Schedule';
            }elseif($pv >= 1){
                $st = 'On Schedule';
            }
        }
        $kegiatan->st = $st;
        $kegiatan->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Kegiatan terupdate',
            'redirect' => 'input',
            'route' => route('phln.kegiatan.edit',$kegiatan->id),
        ]);
    }
    public function destroy(Kegiatan $kegiatan)
    {
        $kegiatan->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Kegiatan terhapus',
        ]);
    }
    public function sync()
    {
        $allkegiatan = Kegiatan::get();
        foreach($allkegiatan AS $kegiatan){
            $today = date('Y-m-d');
            $efektif = $kegiatan->tanggal_efektif;
            $closing = $kegiatan->tanggal_closing;
            $etr1 = $efektif->diffInDays($today);
            $etr2 = $closing->diffInDays($efektif);
            $etr = $etr1/$etr2;
            $penyerapan = PaketAwp::where('kegiatan_id',$kegiatan->id)->orderBy('id','DESC')->first();
            if($penyerapan != null){
                $nilai_penyerapan = PaketAwp::where('kegiatan_id', $kegiatan->id)->sum('real_dana');
                $jumlah_penyerapan = $nilai_penyerapan;
                $mata_uang_kegiatan = MataUang::where('id',$kegiatan->mata_uang_id)->first();
                if($mata_uang_kegiatan->kode != "IDR"){
                    $rate_kegiatan = Rate::where('awal' ,'<=' ,$today)
                    ->where('akhir' ,'>=' ,$today)
                    ->where('mata_uang_id' ,'=' ,$mata_uang_kegiatan->id)
                    ->first();
                    $nilai_kurs_kegiatan = $rate_kegiatan->rate;
                    $jumlah_kegiatan = floatval($kegiatan->nilai)*floatval($nilai_kurs_kegiatan);
                }else{
                    $jumlah_kegiatan = $kegiatan->nilai;
                }
                $dr = $jumlah_penyerapan / $jumlah_kegiatan;
                $pv = $dr/$etr;
                $kegiatan->nilai_konversi = $jumlah_kegiatan;
                $kegiatan->etr = $etr;
                $kegiatan->dr = $dr;
                $kegiatan->pv = $pv;
                $kegiatan->penyerapan = $jumlah_penyerapan;
                $st = '';
                if($dr = 0){
                    if($etr > 0.7){
                        $st = 'At Risk';
                    }elseif($etr <= 0.7){
                        $st = 'Behind Schedule';
                    }
                }else{
                    if($pv <= 0.3 || $etr > 0.7){
                        $st = 'At Risk';
                    }elseif($pv >= 0.3 AND $pv <= 1 || $etr <= 0.7){
                        $st = 'Behind Schedule';
                    }elseif($pv >= 1){
                        $st = 'On Schedule';
                    }
                }
                $kegiatan->st = $st;
                $kegiatan->update();
            }
        }
        return response()->json([
            'alert' => 'success',
            'message' => 'Sinkronasi Berhasil',
        ]);
    }
}
