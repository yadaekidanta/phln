<?php

namespace App\Http\Controllers\Phln;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:office')->except('do_logout');
    }
    public function index()
    {
        return view('page.app.auth.main');
    }
    public function do_login(Request $request){
        $validator = Validator::make($request->all(), [
            'username' => 'required|max:30',
            'password' => 'required|min:8',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('username')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('username'),
                ]);
            }else{
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('password'),
                ]);
            }
        }
        if(Auth::guard('office')->attempt(['username' => $request->username, 'password' => $request->password, 'st' => 'aktif'], $request->remember))
        {
            return response()->json([
                'alert' => 'success',
                'message' => 'Selamat datang '. Auth::guard('office')->user()->nama,
                'callback' => 'reload',
            ]);
        }else{
            return response()->json([
                'alert' => 'error',
                'message' => 'Maaf, sepertinya ada beberapa kesalahan yang terdeteksi, silakan coba lagi.',
            ]);
        }
    }
    public function do_register(Request $request){
        $validator = Validator::make($request->all(), [
            'nama' => 'required|max:100',
            'jabatan' => 'required|max:255',
            'username' => 'required|unique:pgsql.hrm.users|max:30',
            'email' => 'required|unique:pgsql.hrm.users|email|max:255',
            'password' => 'required|min:8',
            'sektor' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('nama')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nama'),
                ]);
            }elseif ($errors->has('jabatan')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('jabatan'),
                ]);
            }elseif ($errors->has('username')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('username'),
                ]);
            }elseif ($errors->has('email')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }elseif ($errors->has('password')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('password'),
                ]);
            }elseif ($errors->has('sektor')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('sektor'),
                ]);
            }
        }
        $data = new User;
        $data->nama = $request->nama;
        $data->jabatan = $request->jabatan;
        $data->username = $request->username;
        $data->email = $request->email;
        $data->password = Hash::make($request->password);
        $data->sektor = $request->sektor;
        $data->st = 'tidak aktif';
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Akun berhasil terdaftar, harap menunggu verifikasi admin',
            'callback' => 'page_login',
        ]);
    }
    public function do_logout(){
        $user = Auth::guard('office')->user();
        Auth::logout($user);
        return redirect()->route('phln.auth.index');
    }
}
