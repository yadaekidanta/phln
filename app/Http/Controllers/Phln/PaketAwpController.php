<?php

namespace App\Http\Controllers\Phln;

use App\Models\Rate;
use App\Models\Paket;
use App\Models\Kegiatan;
use App\Models\MataUang;
use App\Models\PaketAwp;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class PaketAwpController extends Controller
{
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'ta' => 'required',
            'quartal' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('ta')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('ta'),
                ]);
            }elseif($errors->has('quartal')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('quartal'),
                ]);
            }
        }
        $paket = Paket::where('id',$request->paket_id)->first();
        if($request->id){
            $data = PaketAwp::where('id',$request->id)->first();
        }else{
            $data = new PaketAwp;
        }
        $data->kegiatan_id = $request->kegiatan_id;
        $data->paket_id = $request->paket_id;
        $data->ta = $request->ta;
        $data->quartal = $request->quartal;
        $data->masalah = $request->masalah;
        $data->tindak_lanjut = $request->tindak_lanjut;
        $data->target_dana = str_replace(',','',$request->target_dana) ?: 0;
        $data->real_dana = str_replace(',','',$request->real_dana) ?: 0;
        $data->target_fisik = str_replace(',','',$request->target_fisik) ?: 0;
        $data->real_fisik = str_replace(',','',$request->real_fisik) ?: 0;
        if($request->id){
            $data->update();
        }else{
            $data->save();
        }
        $kegiatan = Kegiatan::where('id',$request->kegiatan_id)->first();
        $today = date('Y-m-d');
        $efektif = $kegiatan->tanggal_efektif;
        $closing = $kegiatan->tanggal_closing;
        $etr1 = $efektif->diffInDays($today);
        $etr2 = $closing->diffInDays($efektif);
        $etr = $etr1/$etr2;
        $nilai_penyerapan = PaketAwp::where('kegiatan_id', $data->kegiatan_id)->sum('real_dana');
        $jumlah_penyerapan = $nilai_penyerapan;
        $mata_uang_kegiatan = MataUang::where('id',$kegiatan->mata_uang_id)->first();
        if($mata_uang_kegiatan->kode != "IDR"){
            $rate_kegiatan = Rate::where('awal' ,'<=' ,date("Y-m-d"))
            ->where('akhir' ,'>=' ,date("Y-m-d"))
            ->where('mata_uang_id' ,'=' ,$mata_uang_kegiatan->id)
            ->first();
            $nilai_kurs_kegiatan = $rate_kegiatan->rate;
            $jumlah_kegiatan = floatval($kegiatan->nilai)*floatval($nilai_kurs_kegiatan);
        }else{
            $jumlah_kegiatan = $kegiatan->nilai;
        }
        $dr = $jumlah_penyerapan / $jumlah_kegiatan;
        $pv = $dr/$etr;
        $kegiatan->etr = $etr;
        $kegiatan->dr = $dr;
        $kegiatan->pv = $pv;
        $kegiatan->penyerapan = $jumlah_penyerapan;
        $st = '';
        if($dr = 0){
            if($etr > 0.7){
                $st = 'At Risk';
            }elseif($etr <= 0.7){
                $st = 'Behind Schedule';
            }
        }else{
            if($pv <= 0.3 || $etr > 0.7){
                $st = 'At Risk';
            }elseif($pv >= 0.3 AND $pv <= 1 || $etr <= 0.7){
                $st = 'Behind Schedule';
            }elseif($pv >= 1){
                $st = 'On Schedule';
            }
        }
        $kegiatan->st = $st;
        $kegiatan->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Paket AWP tersimpan',
            'redirect' => 'input',
            'route' => route('phln.paket.edit',[$paket->kegiatan_id,$paket->id]),
        ]);
    }
    public function destroy(PaketAwp $paketAwp)
    {
        $paket = Paket::where('id',$paketAwp->paket_id)->first();
        $paketAwp->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Paket AWP terhapus',
            'redirect' => 'input',
            'route' => route('phln.paket.edit',[$paket->kegiatan_id,$paket->id]),
        ]);
    }
}
