<?php

namespace App\Http\Controllers\Phln;

use App\Models\Kegiatan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index()
    {
        if(Auth::guard('office')->user()->role <= 2 || Auth::guard('office')->user()->role == 5){
            // $pnilai_alokasi = Kegiatan::konversi('Pinjaman','nilai');
            $pnilai_alokasi = Kegiatan::where('tipe_kegiatan','=','Pinjaman')->get()->sum('nilai_konversi');
            $pnilai_penyerapan = Kegiatan::where('tipe_kegiatan','=','Pinjaman')->get()->sum('penyerapan');
            $pkegiatan_pinjaman = Kegiatan::where('tipe_kegiatan','=','Pinjaman')->get()->count();
            $ppagu = DB::select(DB::raw('
            SELECT
                SUM("awp"."target_dana") AS "target",
                SUM("awp"."real_dana") AS "real"
            FROM
                "transaction"."kegiatan" AS "tbl"
            LEFT JOIN "transaction"."paket" AS "paket" ON "tbl"."id" = "paket"."kegiatan_id"
            LEFT JOIN "transaction"."paket_awp" AS "awp" ON "awp"."paket_id" = "paket"."id"
            WHERE
                "awp"."ta" = \''.date('Y').'\'
            AND
                "tbl"."tipe_kegiatan" = \''.'Pinjaman'.'\'
            '));
            $hpagu = DB::select(DB::raw('
            SELECT
                SUM("awp"."target_dana") AS "target",
                SUM("awp"."real_dana") AS "real"
            FROM
                "transaction"."kegiatan" AS "tbl"
            LEFT JOIN "transaction"."paket" AS "paket" ON "tbl"."id" = "paket"."kegiatan_id"
            LEFT JOIN "transaction"."paket_awp" AS "awp" ON "awp"."paket_id" = "paket"."id"
            WHERE
                "awp"."ta" = \''.date('Y').'\'
            AND
                "tbl"."tipe_kegiatan" = \''.'Hibah Terencana'.'\'
            '));
            $hnilai_alokasi = Kegiatan::where('tipe_kegiatan','=','Hibah Terencana')->get()->sum('nilai_konversi');
            $hnilai_penyerapan = Kegiatan::where('tipe_kegiatan','=','Hibah Terencana')->get()->sum('penyerapan');
            $hkegiatan_pinjaman = Kegiatan::where('tipe_kegiatan','=','Hibah Terencana')->get()->count();
            return view('page.app.dashboard.main',compact('pkegiatan_pinjaman','pnilai_alokasi','pnilai_penyerapan','ppagu','hpagu','hkegiatan_pinjaman','hnilai_alokasi','hnilai_penyerapan'));
        }else{
            return redirect()->route('phln.kegiatan.index');
        }
    }
    public function pendanaan(Request $request)
    {
        if ($request->ajax()) {
            $q_donor = DB::select(DB::raw('
            SELECT
                SUM( "tbl"."nilai_konversi" ) - SUM ( "tbl"."penyerapan" ) AS "alokasi",
                SUM ( "tbl"."penyerapan" ) AS "penyerapan",
                "donor"."singkatan" as "donor"
            FROM
                "transaction"."kegiatan" AS "tbl"
            LEFT JOIN "master"."donor" AS "donor" ON "tbl"."donor_id" = "donor"."id"
            WHERE
                "tbl"."tipe_kegiatan" LIKE \'%'.$request->kategori.'%\'
            GROUP BY
                "donor"."singkatan"
            '));
            $arr_d = array();
            foreach($q_donor as $item){
                $temp=array(
                    "alokasi"=>number_format($item->alokasi/1000000000),
                    "penyerapan"=>number_format($item->penyerapan/1000000000),
                    "donor"=>$item->donor
                );
                array_push($arr_d,$temp);
            }
            $donor = json_encode($arr_d);

            $q_sektor = DB::select(DB::raw('
            SELECT
                SUM( "tbl"."nilai_konversi" ) - SUM ( "tbl"."penyerapan" ) AS "alokasi",
                SUM ( "tbl"."penyerapan" ) AS "penyerapan",
                "tbl"."sektor"
            FROM
                "transaction"."kegiatan" AS "tbl"
            WHERE
                "tbl"."tipe_kegiatan" LIKE \'%'.$request->kategori.'%\'
            GROUP BY
                "tbl"."sektor"
            '));
            $arr_s = array();
            $nmsektor = '';
            foreach($q_sektor as $item){
                if($item->sektor == 1){
                    $nmsektor = 'Air Minum';
                }
                elseif($item->sektor == 2){
                    $nmsektor = 'PKP';
                }
                elseif($item->sektor == 3){
                    $nmsektor = 'Sanitasi';
                }
                elseif($item->sektor == 4){
                    $nmsektor = 'BPB';
                }
                elseif($item->sektor == 5){
                    $nmsektor = 'Lintas Sektor';
                }
                $temp=array(
                    "alokasi"=>number_format($item->alokasi/1000000000),
                    "penyerapan"=>number_format($item->penyerapan/1000000000),
                    "nama"=>$nmsektor
                );
                array_push($arr_s,$temp);
            }
            $sektor = json_encode($arr_s);

            $q_kegiatan = DB::select(DB::raw('
            SELECT
                unnest(ARRAY[1,2]) as "type",
                SUM( "tbl"."nilai_konversi" ) - SUM( "tbl"."penyerapan" ) AS "sisa",
                SUM ( "tbl"."penyerapan" ) AS "penyerapan"
            FROM
                "transaction"."kegiatan" AS "tbl"
            WHERE
                "tbl"."tipe_kegiatan" LIKE \'%'.$request->kategori.'%\'
            '));
            $arr_k = array();
            foreach($q_kegiatan as $item){
                $nama = "";
                $jumlah = 0;
                if($item->type == 1){
                    $nama = "Belum terserap";
                    $jumlah = number_format($item->sisa/1000000000);
                }
                if($item->type == 2){
                    $nama = "Penyerapan";
                    $jumlah = number_format($item->penyerapan/1000000000);
                }
                $temp=array(
                    "nama"=>$nama,
                    "jumlah"=>$jumlah
                );
                array_push($arr_k,$temp);
            }
            $kegiatan = json_encode($arr_k);
            return view('page.app.dashboard.list_pendanaan',compact('donor','sektor','kegiatan'));
        }
        return view('page.app.dashboard.pendanaan');
    }
    public function kinerja(Request $request)
    {
        if ($request->ajax()) {
            $q_donor = DB::select(DB::raw('
            SELECT
                SUM(CASE WHEN "tbl"."st" = \'Behind Schedule\' THEN 1 ELSE 0 END) bs,
                SUM(CASE WHEN "tbl"."st" = \'On Schedule\' THEN 1 ELSE 0 END) os,
                SUM(CASE WHEN "tbl"."st" =\'At Risk\' THEN 1 ELSE 0 END) ar,
                "donor"."singkatan" AS "nama"
            FROM
                "transaction"."kegiatan" "tbl"
            LEFT JOIN "master"."donor" AS "donor" ON "donor"."id" = "tbl"."donor_id"
            WHERE
                "tbl"."tipe_kegiatan" LIKE \'%'.$request->kategori.'%\'
            GROUP BY
                "donor"."singkatan"
            '));
            $arr_d = array();

            foreach($q_donor as $item){
                $temp=array(
                    "bs"=>number_format($item->bs),
                    "os"=>number_format($item->os),
                    "ar"=>number_format($item->ar),
                    "nama"=>$item->nama
                );
                array_push($arr_d,$temp);
            }
            $donor = json_encode($arr_d);

            $q_sektor = DB::select(DB::raw('
            SELECT
                SUM(CASE WHEN "tbl"."st" = \'Behind Schedule\' THEN 1 ELSE 0 END) bs,
                SUM(CASE WHEN "tbl"."st" = \'On Schedule\' THEN 1 ELSE 0 END) os,
                SUM(CASE WHEN "tbl"."st" =\'At Risk\' THEN 1 ELSE 0 END) ar,
                "tbl"."sektor"
            FROM
                "transaction"."kegiatan" "tbl"
            WHERE
                "tbl"."tipe_kegiatan" LIKE \'%'.$request->kategori.'%\'
            GROUP BY
                "tbl"."sektor"
            '));
            $arr_s = array();
            $nmsektor = '';
            foreach($q_sektor as $item){
                if($item->sektor == 1){
                    $nmsektor = 'Air Minum';
                }
                elseif($item->sektor == 2){
                    $nmsektor = 'PKP';
                }
                elseif($item->sektor == 3){
                    $nmsektor = 'Sanitasi';
                }
                elseif($item->sektor == 4){
                    $nmsektor = 'BPB';
                }
                elseif($item->sektor == 5){
                    $nmsektor = 'Lintas Sektor';
                }
                $temp=array(
                    "bs"=>number_format($item->bs),
                    "os"=>number_format($item->os),
                    "ar"=>number_format($item->ar),
                    "nama"=>$nmsektor
                );
                array_push($arr_s,$temp);
            }
            $sektor = json_encode($arr_s);

            $q_kegiatan = DB::select(DB::raw('
            SELECT
                COUNT("tbl"."id") AS "total",
                "tbl"."st"
            FROM
                "transaction"."kegiatan" "tbl"
            WHERE
                "tbl"."tipe_kegiatan" LIKE \'%'.$request->kategori.'%\'
            GROUP BY
                "tbl"."st"
            '));
            $arr_k = array();
            foreach($q_kegiatan as $item){
                $temp=array(
                    "total"=>number_format($item->total),
                    "st"=>$item->st
                );
                array_push($arr_k,$temp);
            }
            $kegiatan = json_encode($arr_k);
            return view('page.app.dashboard.list_kinerja',compact('donor','sektor','kegiatan'));
        }
        return view('page.app.dashboard.kinerja');
    }
    public function evaluasi_kinerja(Request $request)
    {
        if ($request->ajax()) {
            $q_evaluasi = DB::select(DB::raw('
            SELECT
                "tbl"."nilai_konversi",
                "tbl"."pv",
                "tbl"."judul"
            FROM
                "transaction"."kegiatan" "tbl"
            WHERE
                "tbl"."tipe_kegiatan" LIKE \'%'.$request->kategori.'%\'
            ORDER BY
                "nilai_konversi" DESC
            '));
            $arr_e = array();
            foreach($q_evaluasi as $item){
                $temp=array(
                    "nilai_konversi"=>number_format($item->nilai_konversi),
                    "pv"=>$item->pv,
                    "judul"=>$item->judul
                );
                array_push($arr_e,$temp);
            }
            $evaluasi = json_encode($arr_e);
            return view('page.app.dashboard.list_evaluasi', compact('evaluasi'));
        }
        return view('page.app.dashboard.evaluasi_kinerja');
    }
}