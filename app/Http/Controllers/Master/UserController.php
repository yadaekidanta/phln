<?php

namespace App\Http\Controllers\Master;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index(Request $request){
        if ($request->ajax()) {
            $keywords = strtoupper($request->keyword);
            $collection = User::where('nama','LIKE','%'.$keywords.'%')
            ->orderBy('id', 'ASC')
            ->paginate(10);
            return view('page.app.user.list',compact('collection'));
        }
        return view('page.app.user.main');
    }
    public function create(){
        return view('page.app.user.input', ['data' => new User]);
    }
    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'nama' => 'required|max:100',
            'jabatan' => 'required|max:255',
            'username' => 'required|unique:pgsql.hrm.users|max:30',
            'email' => 'required|unique:pgsql.hrm.users|email|max:255',
            'password' => 'required|min:8',
            'role' => 'required',
            'sektor' => 'required',
            'st' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('nama')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nama'),
                ]);
            }elseif ($errors->has('jabatan')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('jabatan'),
                ]);
            }elseif ($errors->has('username')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('username'),
                ]);
            }elseif ($errors->has('email')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }elseif ($errors->has('password')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('password'),
                ]);
            }elseif ($errors->has('role')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('role'),
                ]);
            }elseif ($errors->has('sektor')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('sektor'),
                ]);
            }else{
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('st'),
                ]);
            }
        }
        $data = new User;
        $data->nama = $request->nama;
        $data->jabatan = $request->jabatan;
        $data->username = $request->username;
        $data->email = $request->email;
        $data->password = Hash::make($request->password);
        $data->role = $request->role;
        $data->sektor = $request->sektor;
        $data->st = $request->st;
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'User tersimpan',
        ]);
    }
    public function show(User $user){
    }
    public function edit(User $user){
        return view('page.app.user.input', ['data' => $user]);
    }
    public function update(Request $request, User $user){
        $validator = Validator::make($request->all(), [
            'nama' => 'required|max:100',
            'jabatan' => 'required|max:255',
            'username' => 'required|max:30',
            'email' => 'required|email|max:255',
            'role' => 'required',
            'sektor' => 'required',
            'st' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('nama')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nama'),
                ]);
            }elseif ($errors->has('jabatan')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('jabatan'),
                ]);
            }elseif ($errors->has('username')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('username'),
                ]);
            }elseif ($errors->has('email')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }elseif ($errors->has('role')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('role'),
                ]);
            }elseif ($errors->has('sektor')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('sektor'),
                ]);
            }else{
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('st'),
                ]);
            }
        }
        $user->nama = $request->nama;
        $user->jabatan = $request->jabatan;
        $user->username = $request->username;
        $user->email = $request->email;
        if($request->password){
            $user->password = Hash::make($request->password);
        }
        $user->role = $request->role;
        $user->sektor = $request->sektor;
        $user->st = $request->st;
        $user->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'User terubah',
        ]);
    }
    public function destroy(User $user){
        $user->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'User terhapus',
        ]);
    }
    public function aktif(User $user){
        $user->st = "aktif";
        $user->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'User diaktifkan',
        ]);
    }
    public function taktif(User $user){
        $user->st = "tidak aktif";
        $user->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'User tidak diaktifkan',
        ]);
    }
}
