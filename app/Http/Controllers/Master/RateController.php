<?php

namespace App\Http\Controllers\Master;

use App\Models\Rate;
use App\Models\MataUang;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class RateController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = strtoupper($request->keyword);
            $collection = Rate::where('rate','LIKE','%'.$keywords.'%')
            ->orderBy('id', 'ASC')
            ->paginate(10);
            return view('page.app.rate.list',compact('collection'));
        }
        return view('page.app.rate.main');
    }

    public function create()
    {
        $matauang = MataUang::get();
        return view('page.app.rate.input', ['data' => new Rate, 'matauang'=> $matauang]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'matauang' => 'required',
            'awal' => 'required',
            'akhir' => 'required',
            'rate' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('matauang')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('matauang'),
                ]);
            }elseif($errors->has('awal')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('awal'),
                ]);
            }elseif($errors->has('akhir')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('akhir'),
                ]);
            }elseif($errors->has('rate')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('rate'),
                ]);
            }
        }
        $data = new Rate;
        $data->mata_uang_id = $request->matauang;
        $data->awal = $request->awal;
        $data->akhir = $request->akhir;
        $data->rate = $request->rate;
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Rate tersimpan',
        ]);
    }

    public function show(Rate $rate)
    {
        //
    }

    public function edit(Rate $rate)
    {
        $matauang = MataUang::get();
        return view('page.app.rate.input', ['data' => $rate, 'matauang'=> $matauang]);
    }

    public function update(Request $request, Rate $rate)
    {
        $validator = Validator::make($request->all(), [
            'matauang' => 'required',
            'awal' => 'required',
            'akhir' => 'required',
            'rate' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('matauang')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('matauang'),
                ]);
            }elseif($errors->has('awal')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('awal'),
                ]);
            }elseif($errors->has('akhir')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('akhir'),
                ]);
            }elseif($errors->has('rate')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('rate'),
                ]);
            }
        }
        $rate->mata_uang_id = $request->matauang;
        $rate->awal = $request->awal;
        $rate->akhir = $request->akhir;
        $rate->rate = $request->rate;
        $rate->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Rate terupdate',
        ]);
    }

    public function destroy(Rate $rate)
    {
        $rate->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Rate terhapus',
        ]);
    }
}
