function year(obj) {
    var d = new Date();
    var n = d.getFullYear();
    $('#ta' + obj).datepicker({
        todayHighlight: true,
        autoclose: true,
        format: 'yyyy',
        viewMode: "years",
        minViewMode: "years",
        todayBtn: "linked",
        clearBtn: true,
        endDate: 'n'
    });