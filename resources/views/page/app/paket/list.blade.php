@php
$role = Auth::guard('office')->user()->role;
@endphp
<table class="table table-bordered">
    <thead>
        <tr>
            <th class="text-center">No</th>
            <th class="text-center">Lokasi</th>
            {{-- <th class="text-center">Penarikan</th> --}}
            <th class="text-center">Paket</th>
            <th class="text-center">Alokasi</th>
            <th class="text-center">Nilai Kontrak</th>
            <th class="text-center">Tanggal Kontrak</th>
            <th class="text-center">Status Tender</th>
            <th class="text-center">Penyedia Jasa</th>
            <th class="text-center">Fisik</th>
            <th class="text-center">Keuangan</th>
            {{-- <th class="text-center">Realisasi T1</th> --}}
            <th class="text-center">Aksi</th>
        </tr>
    </thead>
    <tbody>
        @php
        $no = 1;
        $status= "";
        @endphp
        @foreach ($collection as $item)
        @php
            $status = $item->st_tender;
            if($status == 0){
                $status = "Belum";
            }else if($status == 1){
                $status = "Proses";
            }else if($status == 2){
                $status =  "Kontrak";
            }
        @endphp
        <tr>
            <td>{{$no++}}</td>
            <td>
                @if($item->prov_id)
                {{$item->provinsi->nm_prov}}, {{$item->kabupaten->nm_kab}}
                @endif
            </td>
            {{-- <td>{{$item->penarikan->nama}}</td> --}}
            <td>({{$item->kode_paket}}) - {{$item->nama_paket}}</td>
            <td>{{number_format($item->alokasi)}}</td>
            <td>{{number_format($item->nilai_kontrak)}}</td>
            <td>
                @if ($item->tanggal_mkontrak)
                    Mulai :{{$item->tanggal_mkontrak->format('j F Y')}}
                    Selesai :{{$item->tanggal_skontrak->format('j F Y')}}
                @endif
            </td>
            <td>{{$status}}</td>
            <td>{{$item->penyedia_jasa}}</td>
            <td>{{number_format($item->last_fisik()->real_fisik,2)}} %</td>
            <td>{{number_format($item->last_keu(),2)}} %</td>
            {{-- <td>{{$item->realisasi_t1}}</td> --}}
            <td>
                @if ($role >= 3)
                <a href="javascript:;" 
                onclick="load_input('{{route('phln.paket.edit',[$kegiatan->id,$item->id])}}');" 
                class="btn btn-icon btn-outline-warning btn-sm mr-2">
                    <i class="flaticon2-edit"></i>
                </a>
                @endif
                @if ($role == 3 || $role == 5)
                <a href="javascript:;" 
                    onclick="handle_confirm('Konfirmasi hapus data','Ya','Tidak','DELETE','{{route('phln.paket.destroy',$item->id)}}');" 
                    class="btn btn-icon btn-outline-danger btn-sm mr-2">
                    <i class="flaticon2-trash"></i>
                </a>
            @endif
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{$collection->links('theme.app.pagination')}}