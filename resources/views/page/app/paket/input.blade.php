<div class="subheader py-3 py-lg-8 subheader-transparent">
    <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <div class="d-flex align-items-center mr-1">
            <div class="d-flex align-items-baseline flex-wrap mr-5">
                <h2 class="d-flex align-items-center text-dark font-weight-bold my-1 mr-3">Pelaksanaan</h2>
                <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold my-2 p-0">
                    <li class="breadcrumb-item text-muted">
                        <a href="javascript:;" class="text-muted">Paket</a>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="javascript:;" class="text-muted">
                            @if ($data->id)
                            Ubah
                            @else
                            Tambah
                            @endif
                            Data Paket
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="d-flex align-items-center flex-wrap">
            <a href="javascript:;" onclick="load_list(1);" class="btn btn-primary btn-fixed-height font-weight-bold px-2 px-lg-5 mr-2" aria-haspopup="true" aria-expanded="false">
                <span class="d-none d-md-inline">Kembali</span>
            </a>
        </div>
    </div>
</div>
<div class="d-flex flex-column-fluid">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <form class="form" id="form_input">
                        <div class="card-body">
                            <h1 class="font-size-lg text-dark font-weight-bold mb-6">Nama Kegiatan : {{$kegiatan->judul}}</h1>
                            <h3 class="font-size-lg text-dark font-weight-bold mb-6">1. Informasi Paket:</h3>
                            <div class="form-group row">
                                <div class="col-lg-4">
                                    <label>Pilih Jenis Paket :</label>
                                    <select class="form-control" name="jenis_paket" id="jenis_paket">
                                        <option value="" selected disabled>Pilih Jenis Paket</option>
                                        <option value="1" {{$data->jenis_paket == 1 ? 'selected' : '' }}>Kontraktual</option>
                                        <option value="2" {{$data->jenis_paket == 2 ? 'selected' : '' }}>Swakelola</option>
                                        <option value="3" {{$data->jenis_paket == 3 ? 'selected' : '' }}>Administrasi Umum</option>
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <input type="hidden" name="kegiatan_id" value="{{$kegiatan->id}}" id="kegiatan_id">
                                    <label>Pilih Provinsi :</label>
                                    <select class="form-control" name="prov_id" id="prov_id">
                                        <option value="">Pilih Provinisi</option>
                                            @foreach($provinsi as $prov)
                                                <option value="{{$prov->id_prov}}" {{$prov->id_prov==$data->prov_id ? 'selected':''}}>{{$prov->nm_prov}}</option>
                                            @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <label>Pilih Kabupaten :</label>
                                    <select class="form-control" name="kab_id" id="kab_id"></select>
                                </div>
                                {{-- <div class="col-lg-4">
                                    <label>Pilih Penarikan :</label>
                                    <select class="form-control" name="penarikan_id" id="penarikan_id">
                                        <option value="">Pilih Penarikan</option>
                                            @foreach($penarikan as $penarikan)
                                                <option value="{{$penarikan->id}}" {{$penarikan->id==$data->penarikan_id ? 'selected':''}}>{{$penarikan->nama}}</option>
                                            @endforeach
                                    </select>
                                </div> --}}
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label>Nama Paket :</label>
                                    <input type="text" value="{{$data->nama_paket}}" name="nama_paket" id="nama_paket" class="form-control" placeholder="Masukan Nama Paket"/>
                                </div>
                                <div class="col-lg-6">
                                    <label>Kode Paket (Sesuai data EMON):</label>
                                    <input type="text" value="{{$data->kode_paket}}" name="kode_paket" id="kode_paket" class="form-control" placeholder="Masukan Kode Paket">
                                </div>
                            </div>
                            <div id="kontraktual">
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <label>Alokasi :</label>
                                        <input type="text" value="{{number_format($data->alokasi)}}" name="alokasi" id="alokasi" class="form-control" placeholder="Masukan Alokasi"/>
                                    </div>
                                    <div class="col-lg-6">
                                        <label>Status Tender :</label>
                                        <select class="form-control" style="width:100%;" name="st_tender" id="st_tender">
                                            <option value="" selected disabled>Pilih Status Tender</option>
                                            <option value="0" {{$data->st_tender == 0 ?'selected':''}}>Belum</option>
                                            <option value="1" {{$data->st_tender == 1 ?'selected':''}}>Proses</option>
                                            <option value="2" {{$data->st_tender == 2 ?'selected':''}}>Kontrak</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <label>Tanggal Mulai Tender <span class="label_tanggal">(Rencana)</span>:</label>
                                        <input type="text" value="{{$data->tanggal_mtender ? $data->tanggal_mtender->format('d-m-Y') : ''}}" name="tanggal_mtender" id="tanggal_mtender" class="form-control" placeholder="Masukkan Tanggal Kontrak" readonly/>
                                    </div>
                                    <div class="col-lg-6">
                                        <label>Tanggal Berakhir Tender <span class="label_tanggal">(Rencana)</span>:</label>
                                        <input type="text" value="{{$data->tanggal_stender ? $data->tanggal_stender->format('d-m-Y') : ''}}" name="tanggal_stender" id="tanggal_stender" class="form-control" placeholder="Masukkan Tanggal Kontrak" readonly/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <label>Tanggal Mulai Kontrak <span class="label_tanggal">(Rencana)</span>:</label>
                                        <input type="text" value="{{$data->tanggal_mkontrak ? $data->tanggal_mkontrak->format('d-m-Y') : ''}}" name="tanggal_mkontrak" id="tanggal_mkontrak" class="form-control" placeholder="Masukkan Tanggal Kontrak" readonly/>
                                    </div>
                                    <div class="col-lg-6">
                                        <label>Tanggal Berakhir Kontrak <span class="label_tanggal">(Rencana)</span>:</label>
                                        <input type="text" value="{{$data->tanggal_skontrak ? $data->tanggal_skontrak->format('d-m-Y') : ''}}" name="tanggal_skontrak" id="tanggal_skontrak" class="form-control" placeholder="Masukkan Tanggal Kontrak" readonly/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <label>Nilai Kontrak :</label>
                                        <input type="tel" value="{{number_format($data->nilai_kontrak)}}" name="nilai_kontrak" id="nilai_kontrak" class="form-control" placeholder="Masukkan Nilai Kontrak"/>
                                    </div>
                                    <div class="col-lg-6">
                                        <label>Penyedia Jasa :</label>
                                        <input type="text" value="{{$data->penyedia_jasa}}" name="penyedia_jasa" id="penyedia_jasa" class="form-control" placeholder="Masukkan Penyedia Jasa"/>
                                    </div>
                                    {{-- <div class="col-lg-4">
                                        <label>Realisasi T1 :</label>
                                        <input type="text" value="{{$data->realisasi_t1}}" name="realisasi_t1" id="realisasi_t1" class="form-control" placeholder="Masukan Realisasi T1"/>
                                    </div> --}}
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-lg-6">
                                    @if ($data->id)
                                    <button id="tombol_simpan" type="button" onclick="handle_save('#tombol_simpan','#form_input','{{route('phln.paket.update',$data->id)}}','PATCH');" class="btn btn-primary mr-2">Ubah</button>
                                    @else    
                                    <button id="tombol_simpan" type="button" onclick="handle_save('#tombol_simpan','#form_input','{{route('phln.paket.store')}}','POST');" class="btn btn-primary mr-2">Simpan</button>
                                    @endif
                                </div>
                                {{-- @if ($data->id)
                                <div class="col-lg-6 text-lg-right">
                                    <button onclick="handle_confirm('Konfirmasi hapus data','Ya','Tidak','DELETE','{{route('phln.paket.destroy',$data->id)}}');" type="button" class="btn btn-danger">Hapus</button>
                                </div>
                                @endif --}}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        @if ($data->id)
        <div class="row">
            <div class="col-xl-12">
                <form id="form_input_awp">
                    <div class="card">
                        <div class="card-body">
                            <h3 class="font-size-lg text-dark font-weight-bold mb-6">Data AWP:</h3>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label>Tahun :</label>
                                    <input type="hidden" id="id" name="id" class="form-control" placeholder="Masukkan Kode Register"/>
                                    <input value="{{$data->kegiatan_id}}" type="hidden" id="kegiatan_id" name="kegiatan_id" class="form-control" placeholder="Masukkan Kode Register"/>
                                    <input value="{{$data->id}}" type="hidden" id="paket_id" name="paket_id" class="form-control" placeholder="Masukkan Kode Register"/>
                                    <input type="text" id="ta" name="ta" class="form-control" placeholder="Masukkan Tahun" readonly/>
                                </div>
                                <div class="col-lg-6">
                                    <label>Quartal :</label>
                                    <select class="form-control" id="quartal" name="quartal">
                                        <option value="1">Q1</option>
                                        <option value="2">Q2</option>
                                        <option value="3">Q3</option>
                                        <option value="4">Q4</option>
                                    </select>
                                </div>
                                <div class="col-lg-6">
                                    <label>Target Dana :</label>
                                    <input type="text" name="target_dana" id="target_dana" class="form-control" placeholder="Masukkan Target Dana"/>
                                </div>
                                <div class="col-lg-6">
                                    <label>Real Dana :</label>
                                    <input type="text" name="real_dana" id="real_dana" class="form-control" placeholder="Masukkan Real Dana"/>
                                </div>
                                <div class="col-lg-6">
                                    <label>Target Fisik (%):</label>
                                    <input type="text" name="target_fisik" id="target_fisik" class="form-control" placeholder="Masukkan Target Fisik"/>
                                </div>
                                <div class="col-lg-6">
                                    <label>Real Fisik (%):</label>
                                    <input type="text" name="real_fisik" id="real_fisik" class="form-control" placeholder="Masukkan Real Fisik"/>
                                </div>
                                <div class="col-lg-6">
                                    <label>Masalah :</label>
                                    <textarea class="form-control" name="masalah" id="masalah"></textarea>
                                </div>
                                <div class="col-lg-6">
                                    <label>Tindak Lanjut :</label>
                                    <textarea class="form-control" name="tindak_lanjut" id="tindak_lanjut"></textarea>
                                </div>
                                <div class="col-lg-4 text-lg-right mt-8">
                                    <button id="tombol_simpan_awp" type="button" onclick="handle_save('#tombol_simpan_awp','#form_input_awp','{{route('phln.paket-awp.store')}}','POST');" class="btn btn-primary mr-2">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Tahun Anggaran</th>
                                        <th>Quartal</th>
                                        <th>Target & Real Dana</th>
                                        <th>Target & Real Fisik</th>
                                        <th>Masalah</th>
                                        <th>Tindak Lanjut</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $no = 1;
                                    @endphp
                                    @foreach ($data->paketAwp as $item)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$item->ta}}</td>
                                        <td>{{$item->quartal}}</td>
                                        <td>
                                            {{number_format($item->target_dana)}}<br>
                                            {{number_format($item->real_dana)}}
                                        </td>
                                        <td>
                                            {{number_format($item->target_fisik)}}<br>
                                            {{number_format($item->real_fisik)}}
                                        </td>
                                        <td>{{$item->masalah}}</td>
                                        <td>{{$item->tindak_lanjut}}</td>
                                        <td>
                                            <a href="javascript:;" onclick="load_edit('{{$item->id}}','{{$item->kegiatan_id}}','{{$item->paket_id}}','{{$item->ta}}','{{$item->quartal}}','{{$item->target_dana}}','{{$item->real_dana}}','{{$item->target_fisik}}','{{$item->real_fisik}}','{{$item->masalah}}','{{$item->tindak_lanjut}}','{{$item->nilai}}');" class="btn btn-icon btn-outline-warning btn-sm mr-2">
                                                <i class="flaticon2-edit"></i>
                                            </a>
                                            <a href="javascript:;" onclick="handle_confirm('Konfirmasi hapus data','Ya','Tidak','DELETE','{{route('phln.paket-awp.destroy',$item->id)}}');" class="btn btn-icon btn-outline-danger btn-sm mr-2">
                                                <i class="flaticon2-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        @endif
    </div>
</div>
<script>
    $('#kontraktual').hide();
    @if($data->st_tender < 2)
    $('.label_tanggal').show();
    @else
    $('.label_tanggal').hide();
    @endif
    @if($data->jenis_paket == 1)
    $('#kontraktual').show();
    @else
    $('#kontraktual').hide();
    @endif
    number_only('id_paket');
    number_only('alokasi');
    number_only('target_dana');
    number_only('real_dana');
    decimal_only('target_fisik');
    decimal_only('real_fisik');
    ribuan('alokasi');
    ribuan('target_dana');
    ribuan('real_dana');
    number_only('nilai_kontrak');
    ribuan('nilai_kontrak');
    select2('penarikan','Pilih Penarikan');
    $("#tanggal_mkontrak").datepicker({
        todayHighlight: true,
        autoclose: true,
        format: 'dd-mm-yyyy',
        orientation: "bottom left",
    }).on('changeDate', function (selected) {
        var startDate = new Date(selected.date.valueOf());
        $('#tanggal_skontrak').datepicker('setStartDate', startDate);
    }).on('clearDate', function (selected) {
        $('#tanggal_skontrak').datepicker('setStartDate', null);
    });
    $("#tanggal_skontrak").datepicker({
        todayHighlight: true,
        autoclose: true,
        format: 'dd-mm-yyyy',
        orientation: "bottom left",
    })
    
    $("#tanggal_mtender").datepicker({
        todayHighlight: true,
        autoclose: true,
        format: 'dd-mm-yyyy',
        orientation: "bottom left",
    }).on('changeDate', function (selected) {
        var startDate = new Date(selected.date.valueOf());
        $('#tanggal_stender').datepicker('setStartDate', startDate);
    }).on('clearDate', function (selected) {
        $('#tanggal_stender').datepicker('setStartDate', null);
    });
    $("#tanggal_stender").datepicker({
        todayHighlight: true,
        autoclose: true,
        format: 'dd-mm-yyyy',
        orientation: "bottom left",
    })
    year('tahun_pelaksanaan');
    year('ta');
   
    select2('prov_id','Pilih Provinsi');
    select2('kab_id','Pilih Provinsi dulu');
    @if($data->prov_id)
    $('#prov_id').val('{{$data->prov_id}}');
    setTimeout(function(){ 
        $('#prov_id').trigger('change');
        setTimeout(function(){ 
            $('#kab_id').val('{{$data->kab_id}}');
            $('#kab_id').trigger('change');
        }, 1200);
    }, 500);
    @endif
    $("#prov_id").change(function(){
        $.ajax({
            type: "POST",
            url: "{{route('phln.get_city')}}",
            data: {id_prov : $("#prov_id").val()},
            success: function(response){
                $("#kab_id").html(response);
            }
        });
    });
    function load_edit(id,kegiatan_id, paket_id,ta,quartal,target_dana,real_dana,target_fisik,real_fisik,masalah,tindak_lanjut){
        $("#id").val(id);
        $("#kegiatan_id").val(kegiatan_id);
        $("#paket_id").val(paket_id);
        $("#ta").val(ta);
        $("#quartal").val(quartal);
        $("#target_dana").val(format_ribuan(target_dana));
        $("#real_dana").val(format_ribuan(real_dana));
        $("#target_fisik").val(format_ribuan(target_fisik));
        $("#real_fisik").val(format_ribuan(real_fisik));
        $("#masalah").html(masalah);
        $("#tindak_lanjut").html(tindak_lanjut);
    }
    // let jpaket = $('#jenis_paket'),
    // kontraktual = $('#kontraktual');
    $('#st_tender').change(function(){
        if(this.value < 2){
            $('.label_tanggal').show();
        }else{
            $('.label_tanggal').hide();
        }
    });
    $('#jenis_paket').change(function(){
        if(this.value == 1){
            $('#kontraktual').show();
        }else{
            $('#kontraktual').hide();
        }
    });
</script>