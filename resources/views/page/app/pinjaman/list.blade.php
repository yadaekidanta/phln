<table class="table">
    <thead>
        <tr>
            <th>No</th>
            <th>No Loan</th>
            <th>Donor</th>
            <th>Nilai</th>
            <th>Mata Uang</th>
            <th>Tanggal Efektif</th>
            <th>Tanggal Closing</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        @php
        $no = 1;
        @endphp
        @foreach ($collection as $item)
        <tr>
            <td>{{$no++}}</td>
            <td>{{$item->no_loan}}</td>
            <td>{{$item->donor->nama}}</td>
            <td>Rp {{number_format($item->nilai)}}</td>
            <td>{{$item->mata_uang->kode}}</td>
            <td>{{$item->tanggal_efektif->format('j F Y')}}</td>
            <td>{{$item->tanggal_closing->format('j F Y')}}</td>
            <td>
                <a href="javascript:;" onclick="load_input('{{route('phln.pinjaman.edit',$item->id)}}');" class="btn btn-icon btn-outline-warning btn-sm mr-2">
                    <i class="flaticon2-edit"></i>
                </a>
                <a href="javascript:;" onclick="handle_confirm('Konfirmasi hapus data','Ya','Tidak','DELETE','{{route('phln.pinjaman.destroy',$item->id)}}');" class="btn btn-icon btn-outline-danger btn-sm mr-2">
                    <i class="flaticon2-trash"></i>
                </a>
                <a href="{{route('phln.pinjaman.kegiatan',$item->id)}}" class="btn btn-icon btn-outline-info btn-sm mr-2">
                    <i class="flaticon-eye"></i>
                </a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{$collection->links('theme.app.pagination')}}