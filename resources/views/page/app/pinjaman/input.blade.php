<div class="subheader py-3 py-lg-8 subheader-transparent">
    <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <div class="d-flex align-items-center mr-1">
            <div class="d-flex align-items-baseline flex-wrap mr-5">
                <h2 class="d-flex align-items-center text-dark font-weight-bold my-1 mr-3">Pelaksanaan</h2>
                <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold my-2 p-0">
                    <li class="breadcrumb-item text-muted">
                        <a href="javascript:;" class="text-muted">Pinjaman</a>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="javascript:;" class="text-muted">List Data</a>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="javascript:;" class="text-muted">
                            @if ($data->id)
                            Ubah
                            @else
                            Tambah
                            @endif
                            Data
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="d-flex align-items-center flex-wrap">
            <a href="javascript:;" onclick="load_list(1);" class="btn btn-primary btn-fixed-height font-weight-bold px-2 px-lg-5 mr-2" aria-haspopup="true" aria-expanded="false">
                <span class="d-none d-md-inline">Kembali</span>
            </a>
        </div>
    </div>
</div>
<div class="d-flex flex-column-fluid">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <form class="form" id="form_input">
                        <div class="card-body">
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label>No Loan :</label>
                                    <input type="text" value="{{$data->no_loan}}" name="no_loan" id="no_loan" class="form-control" placeholder="Masukkan No Loan"/>
                                </div>
                                <div class="col-lg-6">
                                    <label>Donor :</label>
                                    <select class="form-control" style="width:100%;" name="donor" id="donor">
                                        <option value="" selected disabled>Pilih Donor</option>
                                        @foreach ($donor as $item)
                                            <option value="{{$item->id}}" {{$item->id==$data->donor_id?'selected':''}}>{{$item->nama}} | {{$item->singkatan}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label>Mata Uang :</label>
                                    <select class="form-control" style="width:100%;" name="mata_uang" id="mata_uang">
                                        <option value="" selected disabled>Pilih Mata Uang</option>
                                        @foreach ($mata_uang as $item)
                                            <option value="{{$item->id}}" {{$item->id==$data->mata_uang_id?'selected':''}}>{{$item->kode}} | {{$item->nama}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-6">
                                    <label>Nilai :</label>
                                    <input type="tel" value="{{number_format($data->nilai)}}" name="nilai" id="nilai" class="form-control" placeholder="Masukkan Nilai"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label>Tanggal Efektif :</label>
                                    <input type="text" value="{{$data->tanggal_efektif ? $data->tanggal_efektif->format('d-m-Y') : ''}}" name="tanggal_efektif" id="tanggal_efektif" class="form-control" placeholder="Masukkan Tanggal Efektif" readonly/>
                                </div>
                                <div class="col-lg-6">
                                    <label>Tanggal Closing :</label>
                                    <input type="text" value="{{$data->tanggal_closing ? $data->tanggal_closing->format('d-m-Y') : ''}}" name="tanggal_closing" id="tanggal_closing" class="form-control" placeholder="Masukkan Tanggal Closing" readonly/>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-lg-6">
                                    @if ($data->id)
                                    <button id="tombol_simpan" type="button" onclick="handle_save('#tombol_simpan','#form_input','{{route('phln.pinjaman.update',$data->id)}}','PATCH');" class="btn btn-primary mr-2">Ubah</button>
                                    @else    
                                    <button id="tombol_simpan" type="button" onclick="handle_save('#tombol_simpan','#form_input','{{route('phln.pinjaman.store')}}','POST');" class="btn btn-primary mr-2">Simpan</button>
                                    @endif
                                </div>
                                @if ($data->id)
                                <div class="col-lg-6 text-lg-right">
                                    <button onclick="handle_confirm('Konfirmasi hapus data','Ya','Tidak','DELETE','{{route('phln.pinjaman.destroy',$data->id)}}');" type="button" class="btn btn-danger">Hapus</button>
                                </div>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    number_only('nilai');
    ribuan('nilai');
    select2('donor','Pilih Donor');
    select2('mata_uang','Pilih Mata Uang');
    $("#tanggal_efektif").datepicker({
        todayHighlight: true,
        autoclose: true,
        format: 'dd-mm-yyyy',
        orientation: "bottom left",
    }).on('changeDate', function (selected) {
        var startDate = new Date(selected.date.valueOf());
        $('#tanggal_closing').datepicker('setStartDate', startDate);
    }).on('clearDate', function (selected) {
        $('#tanggal_closing').datepicker('setStartDate', null);
    });

    $("#tanggal_closing").datepicker({
        todayHighlight: true,
        autoclose: true,
        format: 'dd-mm-yyyy',
        orientation: "bottom left",
    });
    // datepicker('tanggal_efektif');
    // start_date('tanggal_efektif','tanggal_closing');
    // datepicker('tanggal_closing');
</script>