<x-app-layout title="Dashboard">
    <div id="content_list">
        <!--begin::Subheader-->
        <div class="subheader py-3 py-lg-8 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h2 class="d-flex align-items-center text-dark font-weight-bold my-1 mr-3">Dashboard</h2>
                        <form id="content_filter">
                            <select class="form-control" name="kategori" onchange="load_list(1);">
                                <option value="">Pilih Kategori</option>
                                <option value="Pinjaman">Pinjaman</option>
                                <option value="Hibah Terencana">Hibah Terencana</option>
                            </select>
                        </form>
                        <!--end::Page Title-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->
                <div class="d-flex align-items-center flex-wrap">
                </div>
                <!--end::Toolbar-->
            </div>
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Dashboard-->
                <!--begin::Row-->
                <div class="row">
                    <div class="d-flex align-items-center flex-wrap">
                        <ul class="nav nav-tabs" id="paket" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link {{request()->is('phln/dashboard') ? 'active' : ''}}" href="{{route('phln.dashboard.index')}}">
                                    {{-- <span class="nav-icon">
                                        <i class="flaticon2-chat-1"></i>
                                    </span> --}}
                                    <span class="nav-text">Ringkasan</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{request()->is('phln/dashboard-pendanaan') ? 'active' : ''}}" href="{{route('phln.dashboard.pendanaan')}}">
                                    <span class="nav-text">Pendanaan & Realisasi</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{request()->is('phln/dashboard-kinerja') ? 'active' : ''}}" href="{{route('phln.dashboard.kinerja')}}">
                                    <span class="nav-text">Kinerja Penyerapan</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{request()->is('phln/dashboard-evaluasi-kinerja') ? 'active' : ''}}" href="{{route('phln.dashboard.evaluasi_kinerja')}}">
                                    <span class="nav-text">Evaluasi Kinerja Penyerapan</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div id="list_result"></div>
                <!--end::Row-->
                <!--end::Dashboard-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    @section('custom_js')
    <script>
        load_list(1);
    </script>
    @endsection
</x-app-layout>