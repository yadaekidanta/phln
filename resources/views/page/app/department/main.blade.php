<x-app-layout title="Departemen">
    <div id="content_list">
        <div class="subheader py-3 py-lg-8 subheader-transparent">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <div class="d-flex align-items-center mr-1">
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <h2 class="d-flex align-items-center text-dark font-weight-bold my-1 mr-3">Master Satker</h2>
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold my-2 p-0">
                            <li class="breadcrumb-item text-muted">
                                <a href="javascript:;" class="text-muted">Departemen</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="javascript:;" class="text-muted">List Data</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="d-flex align-items-center flex-wrap">
                    <form id="content_filter">
                        <input class="form-control" onkeyup="load_list(1);" name="keyword">
                    </form>
                    <a href="javascript:;" onclick="load_input('{{route('phln.department.create')}}');" class="btn btn-primary btn-fixed-height font-weight-bold px-2 px-lg-5 mr-2" aria-haspopup="true" aria-expanded="false">
                        <span class="d-none d-md-inline">Tambah Data</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="d-flex flex-column-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <div id="list_result"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="content_input"></div>
    @section('custom_js')
        <script>
            load_list(1);
        </script>
    @endsection
</x-app-layout>