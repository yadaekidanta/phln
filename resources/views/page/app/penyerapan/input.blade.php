<div class="subheader py-3 py-lg-8 subheader-transparent">
    <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <div class="d-flex align-items-center mr-1">
            <div class="d-flex align-items-baseline flex-wrap mr-5">
                <h2 class="d-flex align-items-center text-dark font-weight-bold my-1 mr-3">Pelaksanaan</h2>
                <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold my-2 p-0">
                    <li class="breadcrumb-item text-muted">
                        <a href="javascript:;" class="text-muted">Penyerapan</a>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="javascript:;" class="text-muted">
                            @if ($data->id)
                            Ubah
                            @else
                            Tambah
                            @endif
                            Data Penyerapan
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="d-flex align-items-center flex-wrap">
            <a href="javascript:;" onclick="load_list(1);" class="btn btn-primary btn-fixed-height font-weight-bold px-2 px-lg-5 mr-2" aria-haspopup="true" aria-expanded="false">
                <span class="d-none d-md-inline">Kembali</span>
            </a>
        </div>
    </div>
</div>
<div class="d-flex flex-column-fluid">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <form class="form" id="form_input">
                        <div class="card-body">
                            <h1 class="font-size-lg text-dark font-weight-bold mb-6">Nama Kegiatan : {{$kegiatan->judul}}</h1>
                            <div class="form-group row">
                                <div class="col-lg-4">
                                    <label>Tanggal :</label>
                                    <input type="text" value="{{$data->tanggal ? $data->tanggal->format('d-m-Y') : ''}}" name="tanggal" id="tanggal" class="form-control" placeholder="Masukkan Tanggal" readonly/>
                                </div>
                                <div class="col-lg-4">
                                    <label>Nilai :</label>
                                    <input type="tel" value="{{number_format($data->nilai)}}" name="nilai" id="nilai" class="form-control" placeholder="Masukkan Nilai"/>
                                </div>
                                <div class="col-lg-4">
                                    <input type="hidden" name="kegiatan_id" value="{{$kegiatan->id}}" id="kegiatan_id">
                                    <label>Pilih Mata Uang :</label>
                                    <select class="form-control" name="mata_uang" id="mata_uang">
                                        <option value="">Pilih Mata Uang</option>
                                            @foreach($mata_uang as $row)
                                                <option value="{{$row->id}}" {{$row->id==$data->mata_uang_id ? 'selected':''}}>{{$row->kode}}</option>
                                            @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-lg-6">
                                    @if ($data->id)
                                    <button id="tombol_simpan" type="button" onclick="handle_save('#tombol_simpan','#form_input','{{route('phln.penyerapan.update',$data->id)}}','PATCH');" class="btn btn-primary mr-2">Ubah</button>
                                    @else    
                                    <button id="tombol_simpan" type="button" onclick="handle_save('#tombol_simpan','#form_input','{{route('phln.penyerapan.store')}}','POST');" class="btn btn-primary mr-2">Simpan</button>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    number_only('nilai');
    ribuan('nilai');
    $("#tanggal").datepicker({
        todayHighlight: true,
        autoclose: true,
        format: 'dd-mm-yyyy',
        orientation: "bottom left",
    });
    select2('mata_uang','Pilih Mata Uang');
</script>