<table class="table table-bordered">
    <thead>
        <tr>
            <th class="text-center">No</th>
            <th class="text-center">Tanggal</th>
            <th class="text-center">Nilai</th>
            <th class="text-center">Mata Uang</th>
            <th class="text-center">Aksi</th>
        </tr>
    </thead>
    <tbody>
        @php
        $no = 1;
        @endphp
        @foreach ($collection as $item)
        <tr>
            <td>{{$no++}}</td>
            <td>
                {{$item->tanggal->format('j F Y')}}
            </td>
            <td>{{number_format($item->nilai)}}</td>
            <td>{{$item->mata_uang->kode}}</td>
            <td>
                <a href="javascript:;" 
                onclick="load_input('{{route('phln.penyerapan.edit',[$kegiatan->id,$item->id])}}');" 
                class="btn btn-icon btn-outline-warning btn-sm mr-2">
                    <i class="flaticon2-edit"></i>
                </a>
                <a href="javascript:;" 
                onclick="handle_confirm('Konfirmasi hapus data','Ya','Tidak','DELETE','{{route('phln.penyerapan.destroy',$item->id)}}');" 
                class="btn btn-icon btn-outline-danger btn-sm mr-2">
                    <i class="flaticon2-trash"></i>
                </a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{$collection->links('theme.app.pagination')}}