<table class="table">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>KL</th>
            <th>Unor</th>
            <th>Unit Kerja</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        @php
        $no = 1;
        @endphp
        @foreach ($collection as $item)
        <tr>
            <td>{{$no++}}</td>
            <td>{{$item->nama}}</td>
            <td>{{$item->kl->nama}}</td>
            <td>{{$item->unor->nama}}</td>
            <td>{{$item->unit_kerja->nama}}</td>
            <td>
                <a href="javascript:;" onclick="load_input('{{route('phln.exe-agen.edit',$item->id)}}');" class="btn btn-icon btn-outline-warning btn-sm mr-2">
                    <i class="flaticon2-edit"></i>
                </a>
                <a href="javascript:;" onclick="handle_confirm('Konfirmasi hapus data','Ya','Tidak','DELETE','{{route('phln.exe-agen.destroy',$item->id)}}');" class="btn btn-icon btn-outline-danger btn-sm mr-2">
                    <i class="flaticon2-trash"></i>
                </a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{$collection->links('theme.app.pagination')}}