<div class="subheader py-3 py-lg-8 subheader-transparent">
    <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <div class="d-flex align-items-center mr-1">
            <div class="d-flex align-items-baseline flex-wrap mr-5">
                <h2 class="d-flex align-items-center text-dark font-weight-bold my-1 mr-3">Master PHLN</h2>
                <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold my-2 p-0">
                    <li class="breadcrumb-item text-muted">
                        <a href="javascript:;" class="text-muted">PMU</a>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="javascript:;" class="text-muted">List Data</a>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="javascript:;" class="text-muted">
                            @if ($data->id)
                            Ubah
                            @else
                            Tambah
                            @endif
                            Data
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="d-flex align-items-center flex-wrap">
            <a href="javascript:;" onclick="load_list(1);" class="btn btn-primary btn-fixed-height font-weight-bold px-2 px-lg-5 mr-2" aria-haspopup="true" aria-expanded="false">
                <span class="d-none d-md-inline">Kembali</span>
            </a>
        </div>
    </div>
</div>
<div class="d-flex flex-column-fluid">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <form class="form" id="form_input">
                        <div class="card-body">
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label>Kode :</label>
                                    <input value="{{$data->kode}}" type="text" id="kode" name="kode" class="form-control" placeholder="Masukkan Kode"/>
                                </div>
                                <div class="col-lg-6">
                                    <label>Nama :</label>
                                    <input type="text" value="{{$data->nama}}" name="nama" id="nama" class="form-control" placeholder="Masukkan Nama"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label>Jabatan :</label>
                                    <input value="{{$data->jabatan}}" type="text" id="jabatan" name="jabatan" class="form-control" placeholder="Masukkan Jabatan"/>
                                </div>
                                <div class="col-lg-6">
                                    <label>Alamat :</label>
                                    <input type="text" value="{{$data->alamat}}" name="alamat" id="alamat" class="form-control" placeholder="Masukkan Alamat"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-4">
                                    <label>Telp :</label>
                                    <input value="{{$data->telp}}" type="tel" id="telp" name="telp" maxlength="13" class="form-control" placeholder="Masukkan Telp"/>
                                </div>
                                <div class="col-lg-4">
                                    <label>Fax :</label>
                                    <input type="tel" value="{{$data->fax}}" name="fax" id="fax" class="form-control" placeholder="Masukkan Fax"/>
                                </div>
                                <div class="col-lg-4">
                                    <label>Email :</label>
                                    <input type="email" value="{{$data->email}}" name="email" id="email" class="form-control" placeholder="Masukkan Email"/>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-lg-6">
                                    @if ($data->id)
                                    <button id="tombol_simpan" type="button" onclick="handle_save('#tombol_simpan','#form_input','{{route('phln.pmu.update',$data->id)}}','PATCH');" class="btn btn-primary mr-2">Ubah</button>
                                    @else    
                                    <button id="tombol_simpan" type="button" onclick="handle_save('#tombol_simpan','#form_input','{{route('phln.pmu.store')}}','POST');" class="btn btn-primary mr-2">Simpan</button>
                                    @endif
                                </div>
                                @if ($data->id)
                                <div class="col-lg-6 text-lg-right">
                                    <button onclick="handle_confirm('Konfirmasi hapus data','Ya','Tidak','DELETE','{{route('phln.pmu.destroy',$data->id)}}');" type="button" class="btn btn-danger">Hapus</button>
                                </div>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    number_only('telp')
    number_only('fax')
</script>