<x-app-layout title="Project Brief">
    <div id="content_list">
        <div class="subheader py-3 py-lg-8 subheader-transparent">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <div class="d-flex align-items-center mr-1">
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <h2 class="d-flex align-items-center text-dark font-weight-bold my-1 mr-3">Project Brief</h2>
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold my-2 p-0">
                            <li class="breadcrumb-item text-muted">
                                <a href="javascript:;" class="text-muted">Kegiatan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="javascript:;" class="text-muted">{{$kegiatan->kode_register}}</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="d-flex align-items-center flex-wrap">
                    <ul class="nav nav-tabs" id="paket" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link {{request()->is('phln/kegiatan/*/paket-project-brief') ? 'active' : ''}}" href="{{route('phln.project_brief',$kegiatan->id)}}">
                                {{-- <span class="nav-icon">
                                    <i class="flaticon2-chat-1"></i>
                                </span> --}}
                                <span class="nav-text">Project Brief</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{request()->is('phln/kegiatan/*/paket') ? 'active' : ''}}" href="{{route('phln.kegiatan.paket',$kegiatan->id)}}">
                                <span class="nav-text">Pemaketan & Statusnya</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{request()->is('phln/kegiatan/*/paket-timeline') ? 'active' : ''}}" href="{{route('phln.paket_timeline',$kegiatan->id)}}">
                                <span class="nav-text">Timeline</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{request()->is('phln/kegiatan/*/paket-owp') ? 'active' : ''}}" href="{{route('phln.paket_owp',$kegiatan->id)}}">
                                <span class="nav-text">OWP</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{request()->is('phln/kegiatan/*/paket-awp') ? 'active' : ''}}" href="{{route('phln.paket_awp',$kegiatan->id)}}">
                                <span class="nav-text">AWP</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="d-flex flex-column-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-xl-7">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <h3 class="font-size-lg text-dark font-weight-bold mb-6">Nama Kegiatan: {{$kegiatan->judul}}</h3>
                                    <ul>
                                        <li>Lender : {{$kegiatan->donor->nama}}</li>
                                        <li>
                                            Executing Agency :
                                            @if($kegiatan->exec_unor_code) 
                                            {{$kegiatan->exec_unor->nama}}
                                            @endif
                                            @if($kegiatan->exec_satker_code)
                                            -
                                            {{$kegiatan->exec_satker->nama}}
                                            @endif
                                        </li>
                                        <li>
                                            Implementing Agency : 
                                            @if($kegiatan->imp_unor_code)
                                            {{$kegiatan->imp_unor->nama}}
                                            @endif
                                            @if($kegiatan->imp_satker_code)
                                            - 
                                            {{$kegiatan->imp_satker->nama}}
                                            @endif
                                        </li>
                                        <li>Masa Efektif : {{$kegiatan->tanggal_efektif->format('j F Y')}} - {{$kegiatan->tanggal_closing->format('j F Y')}}</li>
                                        <li>
                                            Metode Pembayaran :
                                            @if($kegiatan->metode_pembayaran == 1)
                                            PP
                                            @elseif($kegiatan->metode_pembayaran == 2)
                                            PL
                                            @elseif($kegiatan->metode_pembayaran == 3)
                                            LK
                                            @elseif($kegiatan->metode_pembayaran == 4)
                                            RK
                                            @endif
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-xl-4">
                                <div class="card" style="background-color:navy;">
                                    <div class="card-body">
                                        <span style="font-size: 80%;color:white;">
                                            Total Pinjaman Luar Negeri
                                        </span>
                                        <br>
                                        <span style="color:white;font-weight:bold;">
                                            Rp {{number_format($kegiatan->nilai_konversi)}}
                                        </span>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-body">
                                        <span style="font-size: 80%;">Realisasi Kumulatif</span>
                                        <br>
                                        <span style="color:black;font-weight:bold;">
                                            Rp {{number_format($kegiatan->penyerapan)}}
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-8">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>PAGU DIPA {{date("Y")}}</th>
                                                        <th>Realisasi {{date("Y")}}</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>{{number_format($pagu_dipa)}}</td>
                                                        <td>
                                                            {{number_format($realisasi)}}
                                                            <br>
                                                            {{$realisasi / $pagu_dipa * 100}} %
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-body">
                                        <span style="font-size: 80%;">Status Kegiatan:</span>
                                        <br>
                                        <span style="color:black;font-weight:bold;">
                                            {{$kegiatan->st}}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-xl-6">
                                <div class="content_grafik_mini" id="penyerapan"></div>
                            </div>
                            <div class="col-xl-6">
                                <div class="content_grafik_mini" id="elapsed"></div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-xl-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h3 class="font-size-lg text-dark font-weight-bold mb-6">Kinerja Kegiatan Berdasarkan PV</h3>
                                        <div class="content_grafik" id="kinerja_kegiatan"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-5">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <h3 class="font-size-lg text-dark font-weight-bold mb-6">Gambaran Umum:</h3>
                                    {!!$kegiatan->tujuan!!}
                                    {!!$kegiatan->sasaran!!}
                                    {!!$kegiatan->lingkup_kegiatan!!}
                                </div>
                            </div>
                        </div>
                        <div class="card mt-5">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <h3 class="font-size-lg text-dark font-weight-bold mb-6">Komponen Kegiatan:</h3>
                                    {!!$kegiatan->komponen!!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="content_input"></div>
    @section('custom_js')
    <script>
        am4core.ready(function() {
        
            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end
            
            var chart = am4core.create("penyerapan", am4charts.PieChart3D);
            chart.hiddenState.properties.opacity = 0; // this creates initial fade-in
            chart.responsive.enabled = true;
            
            chart.legend = new am4charts.Legend();
            
            chart.data = chart.data = {!!$kegiatans!!};
            
            chart.innerRadius = 50;
            
            var series = chart.series.push(new am4charts.PieSeries3D());
            series.dataFields.value = "jumlah";
            series.dataFields.category = "nama";
        
        });
        am4core.ready(function() {
        
            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end
            
            var chart = am4core.create("elapsed", am4charts.PieChart3D);
            chart.hiddenState.properties.opacity = 0; // this creates initial fade-in
            chart.responsive.enabled = true;
            
            chart.legend = new am4charts.Legend();
            
            chart.data = {!!$kegiatanss!!};
            
            chart.innerRadius = 50;
            
            var series = chart.series.push(new am4charts.PieSeries3D());
            series.dataFields.value = "jumlah";
            series.dataFields.category = "nama";
        
        });
        am4core.ready(function() {

            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end

            // Create chart instance
            var chart = am4core.create("kinerja_kegiatan", am4charts.XYChart);

            // Enable chart cursor
            chart.cursor = new am4charts.XYCursor();
            chart.cursor.lineX.disabled = true;
            chart.cursor.lineY.disabled = true;

            // Enable scrollbar
            chart.scrollbarX = new am4core.Scrollbar();

            // Add data
            chart.data = [
                @php
                $srd = 0;
                $date = '';
                @endphp
                @foreach($kinerja_kegiatan as $item)
                {
                    @php
                    $rd = $item->rd;
                    $srd += $rd;
                    $dr = $srd / $kegiatan->nilai_konversi;
                    if($item->quartal == 1){
                        $date = \Carbon\Carbon::parse(date($item->ta.'-03-30'));
                    }elseif($item->quartal == 2){
                        $date = \Carbon\Carbon::parse(date($item->ta.'-06-30'));
                    }elseif($item->quartal == 3){
                        $date = \Carbon\Carbon::parse(date($item->ta.'-09-30'));
                    }elseif($item->quartal == 4){
                        $date = \Carbon\Carbon::parse(date($item->ta.'-12-30'));
                    }
                    $etr = $date->diffInDays($kegiatan->tanggal_efektif) / $kegiatan->tanggal_closing->diffInDays($kegiatan->tanggal_efektif);
                    $pv = $dr / $etr;
                    $kategori = $item->ta . ' / Quartal ' . $item->quartal;
                    @endphp
                    "pv":{{$pv}},
                    "kategori":"{{$kategori}}",
                },
                @endforeach
            ];

            // Create axes
            var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.renderer.inversed = false;
            categoryAxis.dataFields.category = "kategori";
            // categoryAxis.title.text = "Pagu Alokasi (USD)";
            categoryAxis.title.fontWeight = "bold";

            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

            // Create series
            var series = chart.series.push(new am4charts.LineSeries());
            series.tooltipText = "{date}\n[bold font-size: 17px]value: {valueY}[/]";
            series.dataFields.valueY = "pv";
            series.dataFields.categoryX = "kategori";
            series.strokeDasharray = 3;
            series.strokeWidth = 2
            series.strokeOpacity = 0.3;
            series.strokeDasharray = "3,3"

            var bullet = series.bullets.push(new am4charts.CircleBullet());
            bullet.strokeWidth = 2;
            bullet.stroke = am4core.color("#fff");
            bullet.setStateOnChildren = true;
            bullet.propertyFields.fillOpacity = "opacity";
            bullet.propertyFields.strokeOpacity = "opacity";

            var hoverState = bullet.states.create("hover");
            hoverState.properties.scale = 1.7;

        }); // end am4core.ready()
        </script>
    @endsection
</x-app-layout>