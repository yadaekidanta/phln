<x-app-layout title="Paket AWP">
    <div id="content_list">
        <div class="subheader py-3 py-lg-8 subheader-transparent">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <div class="d-flex align-items-center mr-1">
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <h2 class="d-flex align-items-center text-dark font-weight-bold my-1 mr-3">Daftar Paket AWP</h2>
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold my-2 p-0">
                            <li class="breadcrumb-item text-muted">
                                <a href="javascript:;" class="text-muted">Kegiatan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="javascript:;" class="text-muted">{{$kegiatan->kode_register}}</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="d-flex align-items-center flex-wrap">
                    <ul class="nav nav-tabs" id="paket" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link {{request()->is('phln/kegiatan/*/paket-project-brief') ? 'active' : ''}}" href="{{route('phln.project_brief',$kegiatan->id)}}">
                                {{-- <span class="nav-icon">
                                    <i class="flaticon2-chat-1"></i>
                                </span> --}}
                                <span class="nav-text">Project Brief</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{request()->is('phln/kegiatan/*/paket') ? 'active' : ''}}" href="{{route('phln.kegiatan.paket',$kegiatan->id)}}">
                                <span class="nav-text">Pemaketan & Statusnya</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{request()->is('phln/kegiatan/*/paket-timeline') ? 'active' : ''}}" href="{{route('phln.paket_timeline',$kegiatan->id)}}">
                                <span class="nav-text">Timeline</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{request()->is('phln/kegiatan/*/paket-owp') ? 'active' : ''}}" href="{{route('phln.paket_owp',$kegiatan->id)}}">
                                <span class="nav-text">OWP</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{request()->is('phln/kegiatan/*/paket-awp') ? 'active' : ''}}" href="{{route('phln.paket_awp',$kegiatan->id)}}">
                                <span class="nav-text">AWP</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="d-flex flex-column-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <div id="list_result"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="content_input"></div>
    @section('custom_js')
        <script>
            load_list(1);
        </script>
    @endsection
</x-app-layout>