<table class="table table-bordered">
    <thead>
        <tr>
            <th class="text-center" rowspan='2'>No</th>
            <th class="text-center" rowspan='2'>Kode & Nama Paket</th>
            <th class="text-center" rowspan='2'>Alokasi</th>
            <th class="text-center" rowspan='2'>Realisasi Komulatif (T-1)</th>
            <th class="text-center" rowspan='2'>DIPA (Tahun Berjalan)</th>
            <th class="text-center" colspan='4'>Rencana Penyerapan (Tahun Berjalan)</th>
            <th class="text-center" rowspan='2'>Sisa Alokasi</th>
        </tr>
        <tr>
            <th>Q1</th>
            <th>Q2</th>
            <th>Q3</th>
            <th>Q4</th>
        </tr>
    </thead>
    <tbody>
        @php
        $no = 1;
        $alokasi= 0;
        @endphp
        @foreach ($collection as $item)
        @php
            $alokasi = $item->alokasi - $realisasi - $real;
        @endphp
        <tr>
            <td>{{$no++}}</td>
            <td>({{$item->kode_paket}}) - {{$item->nama_paket}}</td>
            <td>{{number_format($item->alokasi)}}</td>
            <td>{{number_format($realisasi)}}</td>
            <td>{{number_format($dipa)}}</td>
            @if($item->paketAwp->count() > 0)
                @if ($item->q1($item->id))
                <td>
                    {{number_format($item->q1($item->id)->target_dana)}}<br>
                    {{number_format($item->q1($item->id)->real_dana)}}
                </td>
                @else
                <td>0</td>
                @endif
                @if ($item->q2($item->id))
                <td>
                    {{number_format($item->q2($item->id)->target_dana)}}<br>
                    {{number_format($item->q2($item->id)->real_dana)}}
                </td>
                @else
                <td>0</td>
                @endif
                @if ($item->q3($item->id))
                <td>
                    {{number_format($item->q3($item->id)->target_dana)}}<br>
                    {{number_format($item->q3($item->id)->real_dana)}}
                </td>
                @else
                <td>0</td>
                @endif
                @if ($item->q4($item->id))
                <td>
                    {{number_format($item->q4($item->id)->target_dana)}}<br>
                    {{number_format($item->q4($item->id)->real_dana)}}
                </td>
                @else
                <td>0</td>
                @endif
            @else
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>
            @endif
            <td>{{number_format($alokasi)}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
{{$collection->links('theme.app.pagination')}}