<div class="subheader py-3 py-lg-8 subheader-transparent">
    <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <div class="d-flex align-items-center mr-1">
            <div class="d-flex align-items-baseline flex-wrap mr-5">
                <h2 class="d-flex align-items-center text-dark font-weight-bold my-1 mr-3">Master PHLN</h2>
                <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold my-2 p-0">
                    <li class="breadcrumb-item text-muted">
                        <a href="javascript:;" class="text-muted">Rate</a>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="javascript:;" class="text-muted">List Data</a>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="javascript:;" class="text-muted">
                            @if ($data->id)
                            Ubah
                            @else
                            Tambah
                            @endif
                            Data
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="d-flex align-items-center flex-wrap">
            <a href="javascript:;" onclick="load_list(1);" class="btn btn-primary btn-fixed-height font-weight-bold px-2 px-lg-5 mr-2" aria-haspopup="true" aria-expanded="false">
                <span class="d-none d-md-inline">Kembali</span>
            </a>
        </div>
    </div>
</div>
<div class="d-flex flex-column-fluid">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <form class="form" id="form_input">
                        <div class="card-body">
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label>Kode Mata Uang :</label>
                                    <select style="width:100%;" class="form-control" name="matauang" id="matauang">
                                        <option value="">Pilih Mata Uang</option>
                                        @foreach($matauang as $matauang)
                                            <option value="{{$matauang->id}}" {{$matauang->id==$data->mata_uang_id ? 'selected' : ''}}>{{$matauang->nama}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-3">
                                    <label>Berlaku :</label>
                                    <input type="text" readonly value="{{$data->awal ? $data->awal->format('Y-m-d') : ''}}" name="awal" id="awal" class="form-control" placeholder="Masukkan tanggal awal"/>
                                </div>
                                <div class="col-lg-3">
                                    <label>S/d :</label>
                                    <input type="text" readonly value="{{$data->akhir ? $data->akhir->format('Y-m-d') : ''}}" name="akhir" id="akhir" class="form-control" placeholder="Masukkan tanggal akhir"/>
                                </div>
                                <div class="col-lg-6">
                                    <label>Rate terhadap rupiah :</label>
                                    <input type="tel" value="{{$data->rate}}" name="rate" id="rate" class="form-control" placeholder="Masukkan Rate"/>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-lg-6">
                                    @if ($data->id)
                                    <button id="tombol_simpan" type="button" onclick="handle_save('#tombol_simpan','#form_input','{{route('phln.rate.update',$data->id)}}','PATCH');" class="btn btn-primary mr-2">Ubah</button>
                                    @else    
                                    <button id="tombol_simpan" type="button" onclick="handle_save('#tombol_simpan','#form_input','{{route('phln.rate.store')}}','POST');" class="btn btn-primary mr-2">Simpan</button>
                                    @endif
                                </div>
                                @if ($data->id)
                                <div class="col-lg-6 text-lg-right">
                                    <button onclick="handle_confirm('Konfirmasi hapus data','Ya','Tidak','DELETE','{{route('phln.rate.destroy',$data->id)}}');" type="button" class="btn btn-danger">Hapus</button>
                                </div>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    select2('matauang','Pilih Mata Uang');
    datepicker_end('awal');
    datepicker_start('akhir');
    number_only('rate');
</script>