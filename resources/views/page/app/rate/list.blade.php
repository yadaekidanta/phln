<table class="table">
    <thead>
        <tr>
            <th>Kode Mata Uang</th>
            <th>Berlaku</th>
            <th>S/d</th>
            <th>Rate Terhadap Rupiah</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        @php
        $no = 1;
        @endphp
        @foreach ($collection as $item)
        <tr>
            <td>{{$item->matauang->nama}}</td>
            <td>{{$item->awal->format('j F Y')}}</td>
            <td>{{$item->akhir->format('j F Y')}}</td>
            <td>{{number_format($item->rate)}}</td>
            <td>
                <a href="javascript:;" onclick="load_input('{{route('phln.rate.edit',$item->id)}}');" class="btn btn-icon btn-outline-warning btn-sm mr-2">
                    <i class="flaticon2-edit"></i>
                </a>
                <a href="javascript:;" onclick="handle_confirm('Konfirmasi hapus data','Ya','Tidak','DELETE','{{route('phln.rate.destroy',$item->id)}}');" class="btn btn-icon btn-outline-danger btn-sm mr-2">
                    <i class="flaticon2-trash"></i>
                </a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{$collection->links('theme.app.pagination')}}