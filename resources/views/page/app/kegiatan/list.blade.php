@php
$role = Auth::guard('office')->user()->role;
@endphp
<table class="table">
    <thead>
        <tr>
            <th>No</th>
            <th>Judul</th>
            {{-- <th>Tujuan</th> --}}
            <th>Donor</th>
            <th>No Loan</th>
            <th>Kode Register</th>
            <th>Tanggal</th>
            <th>Mata Uang</th>
            <th>Nilai</th>
            <th>ETR</th>
            <th>DR</th>
            <th>PV</th>
            {{-- <th>Executing Agency</th>
            <th>Implementing Agency</th> --}}
            {{-- <th>Jumlah Penarikan</th>
            <th>Kinerja Serapan</th>
            <th>Dipa Pagu</th>
            <th>Dipa Anggaran</th>
            <th>Tahun Anggaran</th> --}}
            <th>Status</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        @php
        $no = 1;
        @endphp
        @foreach ($collection as $item)
        <tr>
            <td>{{$no++}}</td>
            <td>{{$item->judul}}</td>
            {{-- <td>{{$item->tujuan}}</td> --}}
            <td>{{$item->donor->nama}}</td>
            <td>{{$item->no_loan}}</td>
            <td>{{$item->kode_register}}</td>
            <td>
                Efektif :{{$item->tanggal_efektif->format('j M Y')}} <br>
                Closing :{{$item->tanggal_closing->format('j M Y')}} <br>
            </td>
            <td>{{$item->mata_uang->kode}}</td>
            <td>{{number_format($item->nilai)}}</td>
            <td>{{$item->etr ? number_format($item->etr,2) : 0}}</td>
            <td>{{$item->dr ? number_format($item->dr,2) : 0}}</td>
            <td>{{$item->pv ? number_format($item->pv,2) : 0}}</td>
            <td>{{$item->st}}</td>
            {{-- <td>
                @if ($item->exec_kl_id)
                KL Eksternal : {{$item->exec_kl->nama}}
                @endif
                @if ($item->exec_unor_id)
                Unor : {{$item->exec_unor->nama}}
                <br>
                @endif
                @if ($item->exec_satker_id)
                Satker : {{$item->exec_satker->nama}}
                @endif
            </td>
            <td>
                @if ($item->imp_kl_id)
                KL Eksternal : {{$item->imp_kl->nama}}
                @endif
                <br>
                @if ($item->imp_unor_id)
                Unor : {{$item->imp_unor->nama}}
                @endif
                <br>
                @if ($item->imp_satker_id)
                Satker : {{$item->imp_satker->nama}}
                @endif
            </td> --}}
            {{-- <td>Rp. {{number_format($item->jumlah_penarikan)}}</td>
            <td>Rp. {{number_format($item->kinerja_serapan)}}</td>
            <td>Rp. {{number_format($item->dipa_pagu)}}</td>
            <td>Rp. {{number_format($item->dipa_anggaran)}}</td>
            <td>{{$item->ta}}</td> --}}
            <td>
                @if ($role >= 3)
                <a title="Edit" href="javascript:;" onclick="load_input('{{route('phln.kegiatan.edit',$item->id)}}');" class="btn btn-icon btn-outline-warning btn-sm mr-2">
                    <i class="flaticon2-edit"></i>
                </a>
                @endif
                @if ($role == 3 || $role == 5)
                <a title="Hapus" href="javascript:;" onclick="handle_confirm('Konfirmasi hapus data','Ya','Tidak','DELETE','{{route('phln.kegiatan.destroy',$item->id)}}');" class="btn btn-icon btn-outline-danger btn-sm mr-2">
                    <i class="flaticon2-trash"></i>
                </a>
                @endif
                {{-- <a title="Penyerapan" href="{{route('phln.kegiatan.penyerapan',$item->id)}}" class="btn btn-icon btn-outline-success btn-sm mr-2">
                    <i class="flaticon-coins"></i>
                </a> --}}
                <a title="Lihat Paket" href="{{route('phln.kegiatan.paket',$item->id)}}" class="btn btn-icon btn-outline-info btn-sm mr-2">
                    <i class="flaticon-eye"></i>
                </a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{$collection->links('theme.app.pagination')}}