<div class="subheader py-3 py-lg-8 subheader-transparent">
    <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <div class="d-flex align-items-center mr-1">
            <div class="d-flex align-items-baseline flex-wrap mr-5">
                <h2 class="d-flex align-items-center text-dark font-weight-bold my-1 mr-3">Pelaksanaan</h2>
                <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold my-2 p-0">
                    <li class="breadcrumb-item text-muted">
                        <a href="javascript:;" class="text-muted">Kegiatan</a>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="javascript:;" class="text-muted">List Data</a>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="javascript:;" class="text-muted">
                            @if ($data->id)
                            Ubah
                            @else
                            Tambah
                            @endif
                            Data
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="d-flex align-items-center flex-wrap">
            <a href="javascript:;" onclick="load_list(1);" class="btn btn-primary btn-fixed-height font-weight-bold px-2 px-lg-5 mr-2" aria-haspopup="true" aria-expanded="false">
                <span class="d-none d-md-inline">Kembali</span>
            </a>
        </div>
    </div>
</div>
<div class="d-flex flex-column-fluid">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <form class="form" id="form_input">
                        <div class="card-body">
                            <h3 class="font-size-lg text-dark font-weight-bold mb-6">1. Informasi Kegiatan:</h3>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label>Kode Register :</label>
                                    <input value="{{$data->kode_register}}" type="text" id="kode_register" name="kode_register" class="form-control" placeholder="Masukkan Kode Register"/>
                                </div>
                                <div class="col-lg-6">
                                    <label>No Loan :</label>
                                    <input type="text" value="{{$data->no_loan}}" name="no_loan" id="no_loan" class="form-control" placeholder="Masukkan No Loan"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-4">
                                    <label>Donor :</label>
                                    <select class="form-control" style="width:100%;" name="donor" id="donor">
                                        <option value="" selected disabled>Pilih Donor</option>
                                        @foreach ($donor as $item)
                                            <option value="{{$item->id}}" {{$item->id==$data->donor_id?'selected':''}}>{{$item->nama}} | {{$item->singkatan}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <label>Nilai :</label>
                                    <input type="tel" value="{{number_format($data->nilai)}}" name="nilai" id="nilai" class="form-control" placeholder="Masukkan Nilai"/>
                                </div>
                                <div class="col-lg-4">
                                    <label>Mata Uang :</label>
                                    <select class="form-control" style="width:100%;" name="mata_uang" id="mata_uang">
                                        <option value="" selected disabled>Pilih Mata Uang</option>
                                        @foreach ($mata_uang as $item)
                                            <option value="{{$item->id}}" {{$item->id==$data->mata_uang_id?'selected':''}}>{{$item->kode}} | {{$item->nama}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label>Judul :</label>
                                    <input type="text" value="{{$data->judul}}" name="judul" id="judul" class="form-control" placeholder="Masukkan Judul Kegiatan"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label>Tujuan :</label>
                                    <textarea name="tujuan" class="form-control" id="tujuan" cols="30" rows="10" placeholder="Masukkan Tujuan Kegiatan">{{$data->tujuan}}</textarea>
                                </div>
                                <div class="col-lg-6">
                                    <label>Sasaran :</label>
                                    <textarea name="sasaran" class="form-control" id="sasaran" cols="30" rows="10" placeholder="Masukkan Sasaran Kegiatan">{{$data->sasaran}}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label>Komponen :</label>
                                    <textarea name="komponen" class="form-control" id="komponen" cols="30" rows="10" placeholder="Masukkan Komponen Kegiatan">{{$data->komponen}}</textarea>
                                </div>
                                <div class="col-lg-6">
                                    <label>Lingkup Kegiatan :</label>
                                    <textarea name="lingkeg" class="form-control" id="lingkeg" cols="30" rows="10" placeholder="Masukkan Lingkup Kegiatan Kegiatan">{{$data->lingkup_kegiatan}}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label>Tanggal Efektif :</label>
                                    <input type="text" value="{{$data->tanggal_efektif ? $data->tanggal_efektif->format('d-m-Y') : ''}}" name="tanggal_efektif" id="tanggal_efektif" class="form-control" placeholder="Masukkan Tanggal Efektif" readonly/>
                                </div>
                                <div class="col-lg-6">
                                    <label>Tanggal Closing :</label>
                                    <input type="text" value="{{$data->tanggal_closing ? $data->tanggal_closing->format('d-m-Y') : ''}}" name="tanggal_closing" id="tanggal_closing" class="form-control" placeholder="Masukkan Tanggal Closing" readonly/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label>Sektor :</label>
                                    <select class="form-control" name="sektor" id="sektor">
                                        <option value=""selected disabled>Pilih Sektor</option>
                                        <option value="1" {{$data->sektor == 1 ? 'selected' : '' }}>Air Minum</option>
                                        <option value="2" {{$data->sektor == 2 ? 'selected' : '' }}>PKP</option>
                                        <option value="3" {{$data->sektor == 3 ? 'selected' : '' }}>Sanitasi</option>
                                        <option value="4" {{$data->sektor == 4 ? 'selected' : '' }}>BPB</option>
                                        <option value="5" {{$data->sektor == 5 ? 'selected' : '' }}>Lintas Sektor</option>
                                    </select>
                                </div>
                                <div class="col-lg-6">
                                    <label>Metode Pembayaran :</label>
                                    <select class="form-control"name="metode_pembayaran" id="metode_pembayaran">
                                        <option value=""selected disabled>Pilih Metode Pembayaran</option>
                                        <option value="1"  {{$data->metode_pembayaran == 1 ? 'selected' : '' }}>PP</option>
                                        <option value="2"  {{$data->metode_pembayaran == 2 ? 'selected' : '' }}>PL</option>
                                        <option value="3"  {{$data->metode_pembayaran == 3 ? 'selected' : '' }}>LC</option>
                                        <option value="4"  {{$data->metode_pembayaran == 4 ? 'selected' : '' }}>RK</option>
                                    </select>                                
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-lg-6">
                                        @if ($data->id)
                                        <button id="tombol_simpan" type="button" onclick="handle_save('#tombol_simpan','#form_input','{{route('phln.kegiatan.update',$data->id)}}','PATCH');" class="btn btn-primary mr-2">Ubah</button>
                                        @else    
                                        <button id="tombol_simpan" type="button" onclick="handle_save('#tombol_simpan','#form_input','{{route('phln.kegiatan.store')}}','POST');" class="btn btn-primary mr-2">Simpan</button>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            @if ($data->id)
            <div class="col-xl-12">
                <form id="form_input_exec">
                    <div class="card">
                        <div class="card-body">
                        <h3 class="font-size-lg text-dark font-weight-bold mb-6">2. Executing Agency:</h3>
                            <div class="form-group row">
                                <div class="col-lg-3">
                                    <label>Kementrian / Lembaga :</label>
                                    <input type="hidden" name="kegiatan_exec" value="{{$data->id}}">
                                    <select name="exec_kl" id="exec_kl" class="form-control">
                                        <option value="">Pilih Kementrian / Lembaga</option>
                                        @foreach($department as $item)
                                            <option value="{{$item->kode}}">{{$item->nama}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-3">
                                    <label>Unor :</label>
                                    <select name="exec_unor" id="exec_unor" class="form-control">
                                    </select>
                                </div>
                                <div class="col-lg-3">
                                    <label>Satker :</label>
                                    <select name="exec_satker" id="exec_satker" class="form-control">
                                    </select>
                                </div>
                                <div class="col-lg-3 text-lg-right mt-8">
                                    <button id="tombol_simpan_exec" type="button" onclick="handle_save('#tombol_simpan_exec','#form_input_exec','{{route('phln.kegiatan.storeExec')}}','POST');" class="btn btn-primary mr-2">Tambah</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kementrian / Lembaga</th>
                                        <th>Unor</th>
                                        <th>Satker</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $no = 1;
                                    @endphp
                                    @foreach ($data->exec as $item)
                                    <tr>
                                        <td>{{$no}}</td>
                                        <td>{{$item->kl->nama}}</td>
                                        <td>{{$item->unor->nama}}</td>
                                        <td>{{$item->satker->nama}}</td>
                                        <td>
                                            <a href="javascript:;" onclick="handle_confirm('Konfirmasi hapus data','Ya','Tidak','DELETE','{{route('phln.kegiatan.destroyExec',$item->id)}}');" class="btn btn-icon btn-outline-danger btn-sm mr-2">
                                                <i class="flaticon2-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-xl-12">
                <form id="form_input_imp">
                    <div class="card">
                        <div class="card-body">
                            <h3 class="font-size-lg text-dark font-weight-bold mb-6">3. Implementing Agency:</h3>
                            <div class="form-group row">
                                <div class="col-lg-3">
                                    <label>Kementrian / Lembaga :</label>
                                    <input type="hidden" name="kegiatan_imp" value="{{$data->id}}">
                                    <select name="imp_kl" id="imp_kl" class="form-control">
                                        <option value="">Pilih Kementrian / Lembaga</option>
                                        @foreach($department as $item)
                                            <option value="{{$item->kode}}">{{$item->nama}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-3">
                                    <label>Unor :</label>
                                    <select name="imp_unor" id="imp_unor" class="form-control">
                                    </select>
                                </div>
                                <div class="col-lg-3">
                                    <label>Satker :</label>
                                    <select name="imp_satker" id="imp_satker" class="form-control">
                                    </select>
                                </div>
                                <div class="col-lg-3 text-lg-right mt-8">
                                    <button id="tombol_simpan_imp" type="button" onclick="handle_save('#tombol_simpan_imp','#form_input_imp','{{route('phln.kegiatan.storeImp')}}','POST');" class="btn btn-primary mr-2">Tambah</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kementrian / Lembaga</th>
                                        <th>Unor</th>
                                        <th>Satker</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $no = 1;
                                    @endphp
                                    @foreach ($data->imp as $item)
                                    <tr>
                                        <td>{{$no}}</td>
                                        <td>{{$item->kl->nama}}</td>
                                        <td>{{$item->unor->nama}}</td>
                                        <td>{{$item->satker->nama}}</td>
                                        <td>
                                            <a href="javascript:;" onclick="handle_confirm('Konfirmasi hapus data','Ya','Tidak','DELETE','{{route('phln.kegiatan.destroyImp',$item->id)}}');" class="btn btn-icon btn-outline-danger btn-sm mr-2">
                                                <i class="flaticon2-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-xl-12">
                <form id="form_input_mu">
                    <div class="card">
                        <div class="card-body">
                            <h3 class="font-size-lg text-dark font-weight-bold mb-6">Data Management Unit:</h3>
                            <div class="form-group row">
                                <div class="col-lg-3">
                                    <label>Nama :</label>
                                    <input type="hidden" id="id" name="id" class="form-control" placeholder="Masukkan Kode Register"/>
                                    <input value="{{$data->kode_register}}" type="hidden" id="kd_register" name="kd_register" class="form-control" placeholder="Masukkan Kode Register"/>
                                    <input type="text" id="nama" name="nama" class="form-control" placeholder="Masukkan Nama"/>
                                </div>
                                <div class="col-lg-3">
                                    <label>Jabatan :</label>
                                    <input type="text" name="jabatan" id="jabatan" class="form-control" placeholder="Masukkan Jabatan"/>
                                </div>
                                <div class="col-lg-3">
                                    <label>Alamat :</label>
                                    <input type="text" name="alamat" id="alamat" class="form-control" placeholder="Masukkan Alamat"/>
                                </div>
                                <div class="col-lg-3 text-lg-right mt-8">
                                    <button id="tombol_simpan_mu" type="button" onclick="handle_save('#tombol_simpan_mu','#form_input_mu','{{route('phln.management-unit.store')}}','POST');" class="btn btn-primary mr-2">Tambah</button>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-3">
                                    <label>Telp :</label>
                                    <input type="text" id="telp" name="telp" class="form-control" placeholder="Masukkan Telp"/>
                                </div>
                                <div class="col-lg-3">
                                    <label>Fax :</label>
                                    <input type="text" name="fax" id="fax" class="form-control" placeholder="Masukkan Fax"/>
                                </div>
                                <div class="col-lg-3">
                                    <label>Email :</label>
                                    <input type="text" name="email" id="email" class="form-control" placeholder="Masukkan Email"/>
                                </div>
                                <div class="col-lg-3">
                                    <label>Tipe :</label>
                                    <select class="form-control" id="tipe" name="tipe">
                                        <option value="" SELECTED DISABLED>Pilih Tipe</option>
                                        <option value="CPMU">CPMU</option>
                                        <option value="PMU">PMU</option>
                                        <option value="PIU">PIU</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Jabatan</th>
                                        <th>Alamat</th>
                                        <th>Telp</th>
                                        <th>Fax</th>
                                        <th>Email</th>
                                        <th>Tipe</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $no = 1;
                                    @endphp
                                    @foreach ($data->managementUnit as $item)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$item->nama}}</td>
                                        <td>{{$item->jabatan}}</td>
                                        <td>{{$item->alamat}}</td>
                                        <td>{{$item->telp}}</td>
                                        <td>{{$item->fax}}</td>
                                        <td>{{$item->email}}</td>
                                        <td>{{$item->type}}</td>
                                        <td>
                                            <a href="javascript:;" onclick="load_edit('{{$item->id}}',
                                                '{{$item->nama}}',
                                                '{{$item->jabatan}}',
                                                '{{$item->alamat}}',
                                                '{{$item->telp}}',
                                                '{{$item->fax}}',
                                                '{{$item->email}}',
                                                '{{$item->type}}');" class="btn btn-icon btn-outline-warning btn-sm mr-2">
                                                <i class="flaticon2-edit"></i>
                                            </a>
                                            <a href="javascript:;" onclick="handle_confirm('Konfirmasi hapus data','Ya','Tidak','DELETE','{{route('phln.management-unit.destroy',$item->id)}}');" class="btn btn-icon btn-outline-danger btn-sm mr-2">
                                                <i class="flaticon2-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
        @endif
        <h3 class="font-size-lg text-dark font-weight-bold mb-6 d-none">5. Keuangan:</h3>
        <div class="form-group row d-none">
            <div class="col-lg-6">
                <label>Jumlah Penarikan :</label>
                <input type="text" value="{{number_format($data->jumlah_penarikan)}}" name="jumlah_penarikan" id="jumlah_penarikan" class="form-control" placeholder="Masukkan Jumlah Penarikan"/>
            </div>
            <div class="col-lg-6">
                <label>Kinerja Serapan :</label>
                <input type="text" value="{{number_format($data->kinerja_serapan)}}" name="kinerja_serapan" id="kinerja_serapan" class="form-control" placeholder="Masukkan Kinerja Serapan"/>
            </div>
        </div>
        <div class="form-group row d-none">
            <div class="col-lg-6">
                <label>Dipa Pagu :</label>
                <input type="text" value="{{number_format($data->dipa_pagu)}}" name="dipa_pagu" id="dipa_pagu" class="form-control" placeholder="Masukkan Dipa Pagu"/>
            </div>
            <div class="col-lg-6">
                <label>Dipa Anggaran :</label>
                <input type="text" value="{{number_format($data->dipa_anggaran)}}" name="dipa_anggaran" id="dipa_anggaran" class="form-control" placeholder="Masukkan Dipa Anggaran"/>
            </div>
        </div>
        <div class="form-group row d-none">
            <div class="col-lg-4">
                <label>Tahun Anggaran :</label>
                <input type="text" value="{{$data->ta}}" name="ta" id="ta" class="form-control" placeholder="Pilih Tahun Anggaeran" readonly/>
            </div>
        </div>
        {{-- <div class="card-footer">
            <div class="row">
                <div class="col-lg-6">
                    @if ($data->id)
                    <button id="tombol_simpan" type="button" onclick="handle_save('#tombol_simpan','#form_input','{{route('phln.kegiatan.update',$data->id)}}','PATCH');" class="btn btn-primary mr-2">Ubah</button>
                    @else    
                    <button id="tombol_simpan" type="button" onclick="handle_save('#tombol_simpan','#form_input','{{route('phln.kegiatan.store')}}','POST');" class="btn btn-primary mr-2">Simpan</button>
                    @endif
                </div>
            </div>
        </div> --}}
    </div>
</div>
<script type="text/javascript">
    select2('donor','Pilih Donor');
    select2('mata_uang','Pilih Mata Uang');
    $("#tanggal_efektif").datepicker({
        todayHighlight: true,
        autoclose: true,
        format: 'dd-mm-yyyy',
        orientation: "bottom left",
    }).on('changeDate', function (selected) {
        var startDate = new Date(selected.date.valueOf());
        $('#tanggal_closing').datepicker('setStartDate', startDate);
    }).on('clearDate', function (selected) {
        $('#tanggal_closing').datepicker('setStartDate', null);
    });

    $("#tanggal_closing").datepicker({
        todayHighlight: true,
        autoclose: true,
        format: 'dd-mm-yyyy',
        orientation: "bottom left",
    });
    obj_summernote('tujuan');
    obj_summernote('sasaran');
    obj_summernote('komponen');
    obj_summernote('lingkeg');
    number_only('nilai');
    number_only('dipa_anggaran');
    number_only('dipa_pagu');
    number_only('jumlah_penarikan');
    number_only('kinerja_serapan');
    ribuan('nilai');
    ribuan('dipa_anggaran');
    ribuan('dipa_pagu');
    ribuan('jumlah_penarikan')
    ribuan('kinerja_serapan')
    select2('exec_kl','Pilih Kementrian / Lembaga');
    select2('exec_unor','Pilih Kementrian / Lembaga lebih dulu');
    select2('exec_satker','Pilih Unor lebih dulu');
    select2('imp_kl','Pilih Kementrian / Lembaga');
    select2('imp_unor','Pilih Kementrian / Lembaga lebih dulu');
    select2('imp_satker','Pilih Unor lebih dulu');
    year('ta');

    function load_edit(id,nama,jabatan,alamat,telp,fax,email,type){
        $("#id").val(id);
        $("#nama").val(nama);
        $("#jabatan").val(jabatan);
        $("#alamat").val(alamat);
        $("#telp").val(telp);
        $("#fax").val(fax);
        $("#email").val(email);
        $("#tipe").val(type);
    }
    $("#exec_kl").change(function(){
        $.ajax({
            type: "POST",
            url: "{{route('phln.get_unor')}}",
            data: {department_id : $("#exec_kl").val()},
            success: function(response){
                $("#exec_unor").html(response);
            }
        });
    });
    $("#exec_unor").change(function(){
        $.ajax({
            type: "POST",
            url: "{{route('phln.get_satker')}}",
            data: {unor_id : $("#exec_unor").val()},
            success: function(response){
                $("#exec_satker").html(response);
            }
        });
    });

    $("#imp_kl").change(function(){
        $.ajax({
            type: "POST",
            url: "{{route('phln.get_unor')}}",
            data: {department_id : $("#imp_kl").val()},
            success: function(response){
                $("#imp_unor").html(response);
            }
        });
    });
    $("#imp_unor").change(function(){
        $.ajax({
            type: "POST",
            url: "{{route('phln.get_satker')}}",
            data: {unor_id : $("#imp_unor").val()},
            success: function(response){
                $("#imp_satker").html(response);
            }
        });
    });
</script>