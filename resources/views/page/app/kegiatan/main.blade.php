<x-app-layout title="Pinjaman">
    @php
    $role = Auth::guard('office')->user()->role;
    @endphp
    <div id="content_list">
        <div class="subheader py-3 py-lg-8 subheader-transparent">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <div class="d-flex align-items-center mr-1">
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <h2 class="d-flex align-items-center text-dark font-weight-bold my-1 mr-3">Pelaksanaan</h2>
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold my-2 p-0">
                            <li class="breadcrumb-item text-muted">
                                <a href="javascript:;" class="text-muted">Kegiatan</a>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="javascript:;" class="text-muted">List Data</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="d-flex align-items-center flex-wrap">
                    <a href="javascript:;" onclick="handle_confirm('Konfirmasi Sinkronasi data','Ya','Tidak','POST','{{route('phln.kegiatan.sync')}}');" class="btn btn-primary btn-fixed-height font-weight-bold px-2 px-lg-5 mr-2" aria-haspopup="true" aria-expanded="false">
                        <span class="d-none d-md-inline">Sync</span>
                    </a>
                    <form id="content_filter">
                        <input class="form-control" onkeyup="load_list(1);" name="keyword">
                    </form>
                    @if ($role >= 3)
                    <a href="javascript:;" onclick="load_input('{{route('phln.kegiatan.create')}}');" class="btn btn-primary btn-fixed-height font-weight-bold px-2 px-lg-5 mr-2" aria-haspopup="true" aria-expanded="false">
                        <span class="d-none d-md-inline">Tambah Data</span>
                    </a>
                    @endif
                </div>
            </div>
        </div>
        <div class="d-flex flex-column-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-xl-4">
                        <a href="javascript:;" onclick="load_input('{{route('phln.kegiatan.risk')}}');">
                            <div class="card card-custom bg-radial-gradient-danger gutter-b" style="height: 90px">
                                <div class="card-body d-flex flex-column p-0">
                                    <div class="flex-grow-1 card-spacer-x pt-6">
                                        <div class="text-inverse-danger font-weight-bold">At Risk</div>
                                        <div class="text-inverse-danger font-weight-bolder font-size-h3">{{$ar}}</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xl-4">
                        <a href="javascript:;" onclick="load_input('{{route('phln.kegiatan.bs')}}');">
                            <div class="card card-custom bg-radial-gradient-warning gutter-b" style="height: 90px">
                                <div class="card-body d-flex flex-column p-0">
                                    <div class="flex-grow-1 card-spacer-x pt-6">
                                        <div class="text-inverse-warning font-weight-bold">Behind Schedule</div>
                                        <div class="text-inverse-warning font-weight-bolder font-size-h3">{{$bs}}</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xl-4">
                        <a href="javascript:;" onclick="load_input('{{route('phln.kegiatan.os')}}');">
                            <div class="card card-custom bg-radial-gradient-success gutter-b" style="height: 90px">
                                <div class="card-body d-flex flex-column p-0">
                                    <div class="flex-grow-1 card-spacer-x pt-6">
                                        <div class="text-inverse-success font-weight-bold">On Schedule</div>
                                        <div class="text-inverse-success font-weight-bolder font-size-h3">{{$os}}</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <div id="list_result"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="content_input"></div>
    @section('custom_js')
        <script>
            load_list(1);
        </script>
    @endsection
</x-app-layout>