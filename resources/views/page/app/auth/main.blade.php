<x-auth-layout title="Masuk">
    <div class="d-flex flex-column-fluid flex-center">
        <div id="page_login">
            <div class="login-form login-signin">
                <form class="form" novalidate="novalidate" id="form_login">
                    <div class="pb-13 pt-lg-0 pt-5">
                        <h3 class="font-weight-bolder text-dark font-size-h4 font-size-h1-lg">Selamat Datang di {{config('app.name')}}</h3>
                        <span class="text-muted font-weight-bold font-size-h4">Baru disini?
                        <a href="javascript:;" onclick="auth_content('page_register');" class="text-primary font-weight-bolder">Buat akun</a></span>
                    </div>
                    <div class="form-group">
                        <label class="font-size-h6 font-weight-bolder text-dark">Username</label>
                        <input class="form-control form-control-solid h-auto py-6 px-6 rounded-lg" type="text" name="username" autocomplete="off" />
                    </div>
                    <div class="form-group">
                        <div class="d-flex justify-content-between mt-n5">
                            <label class="font-size-h6 font-weight-bolder text-dark pt-5">Kata Sandi</label>
                            <a href="javascript:;" class="text-primary font-size-h6 font-weight-bolder text-hover-primary pt-5" id="kt_login_forgot">Lupa Kata Sandi ?</a>
                        </div>
                        <input class="form-control form-control-solid h-auto py-6 px-6 rounded-lg" type="password" name="password" autocomplete="off" />
                    </div>
                    <div class="pb-lg-0 pb-5">
                        <button id="tombol_login" type="button" onclick="handle_post('#tombol_login','#form_login','{{route('phln.auth.login')}}');" class="btn btn-primary font-weight-bolder font-size-h6 px-8 py-4 my-3 mr-3">Masuk</button>
                    </div>
                </form>
            </div>
        </div>
        <div id="page_register">
            <div class="register-form register-signin">
                <form class="form" novalidate="novalidate" id="form_register">
                    <div class="pb-13 pt-lg-0 pt-5">
                        <h3 class="font-weight-bolder text-dark font-size-h4 font-size-h1-lg">Selamat Datang di {{config('app.name')}}</h3>
                        <span class="text-muted font-weight-bold font-size-h4">Sudah punya akun?
                        <a href="javascript:;" onclick="auth_content('page_login');" class="text-primary font-weight-bolder">Masuk disini</a></span>
                    </div>
                    <div class="form-group">
                        <label class="font-size-h6 font-weight-bolder text-dark">Nama</label>
                        <input class="form-control form-control-solid rounded-lg" type="text" name="nama" autocomplete="off" />
                    </div>
                    <div class="form-group">
                        <label class="font-size-h6 font-weight-bolder text-dark">Username</label>
                        <input class="form-control form-control-solid rounded-lg" type="text" name="username" autocomplete="off" />
                    </div>
                    <div class="form-group">
                        <label class="font-size-h6 font-weight-bolder text-dark">Password</label>
                        <input class="form-control form-control-solid rounded-lg" type="password" name="password" autocomplete="off" />
                    </div>
                    <div class="form-group">
                        <label class="font-size-h6 font-weight-bolder text-dark">Email</label>
                        <input class="form-control form-control-solid rounded-lg" type="email" name="email" autocomplete="off" />
                    </div>
                    <div class="form-group">
                        <label class="font-size-h6 font-weight-bolder text-dark">Jabatan</label>
                        <input class="form-control form-control-solid rounded-lg" type="text" name="jabatan" autocomplete="off" />
                    </div>
                    <div class="form-group">
                        <label class="font-size-h6 font-weight-bolder text-dark">Sektor</label>
                        <select style="width:100%;" class="form-control" name="sektor" id="sektor">
                            <option value="">Pilih Sektor</option>
                            <option value="1">Air Minum</option>
                            <option value="2">PKP</option>
                            <option value="3">Sanitasi</option>
                            <option value="4">BPB</option>
                            <option value="5">Lintas Sektor</option>
                        </select>
                    </div>
                    <div class="pb-lg-0 pb-5">
                        <button id="tombol_register" type="button" onclick="handle_post('#tombol_register','#form_register','{{route('phln.auth.register')}}');" class="btn btn-primary font-weight-bolder font-size-h6 px-8 py-4 my-3 mr-3">Daftar</button>
                    </div>
                </form>
            </div>
        </div>
        <div id="page_forgot">
            <div class="login-form login-forgot">
                <form class="form" novalidate="novalidate" id="form_forgot">
                    <div class="pb-13 pt-lg-0 pt-5">
                        <h3 class="font-weight-bolder text-dark font-size-h4 font-size-h1-lg">Forgotten Password ?</h3>
                        <p class="text-muted font-weight-bold font-size-h4">Enter your email to reset your password</p>
                    </div>
                    <div class="form-group">
                        <input class="form-control form-control-solid h-auto py-6 px-6 rounded-lg font-size-h6" type="email" placeholder="Email" name="email" autocomplete="off" />
                    </div>
                    <div class="form-group d-flex flex-wrap pb-lg-0">
                        <button type="button" id="kt_login_forgot_submit" class="btn btn-primary font-weight-bolder font-size-h6 px-8 py-4 my-3 mr-4">Submit</button>
                        <button type="button" id="kt_login_forgot_cancel" class="btn btn-light-primary font-weight-bolder font-size-h6 px-8 py-4 my-3">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @section('custom_js')
    <script>
        auth_content('page_login');
    </script>
    @endsection
</x-auth-layout>