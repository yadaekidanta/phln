<div class="subheader py-3 py-lg-8 subheader-transparent">
    <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <div class="d-flex align-items-center mr-1">
            <div class="d-flex align-items-baseline flex-wrap mr-5">
                <h2 class="d-flex align-items-center text-dark font-weight-bold my-1 mr-3">Master PHLN</h2>
                <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold my-2 p-0">
                    <li class="breadcrumb-item text-muted">
                        <a href="javascript:;" class="text-muted">User</a>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="javascript:;" class="text-muted">List Data</a>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="javascript:;" class="text-muted">
                            @if ($data->id)
                            Ubah
                            @else
                            Tambah
                            @endif
                            Data
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="d-flex align-items-center flex-wrap">
            <a href="javascript:;" onclick="load_list(1);" class="btn btn-primary btn-fixed-height font-weight-bold px-2 px-lg-5 mr-2" aria-haspopup="true" aria-expanded="false">
                <span class="d-none d-md-inline">Kembali</span>
            </a>
        </div>
    </div>
</div>
<div class="d-flex flex-column-fluid">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <form class="form" id="form_input">
                        <div class="card-body">
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label>Nama :</label>
                                    <input type="text" maxlength="100" value="{{$data->nama}}" name="nama" id="nama" class="form-control" placeholder="Masukkan Nama"/>
                                </div>
                                <div class="col-lg-6">
                                    <label>Jabatan :</label>
                                    <input type="text" value="{{$data->jabatan}}" name="jabatan" id="jabatan" class="form-control" placeholder="Masukkan Jabatan"/>
                                </div>
                                <div class="col-lg-6">
                                    <label>Username :</label>
                                    <input type="text" maxlength="30" value="{{$data->username}}" name="username" id="username" class="form-control" placeholder="Masukkan Username"/>
                                </div>
                                <div class="col-lg-6">
                                    <label>Email :</label>
                                    <input type="email" value="{{$data->email}}" name="email" id="email" class="form-control" placeholder="Masukkan Email"/>
                                </div>
                                <div class="col-lg-6">
                                    <label>Password :</label>
                                    <input type="password" name="password" id="password" class="form-control" placeholder="Masukkan Password"/>
                                </div>
                                <div class="col-lg-3">
                                    <label>Role :</label>
                                    <select style="width:100%;" class="form-control" name="role" id="role">
                                        <option value="">Pilih Role</option>
                                        <option value="1" {{$data->role==1 ? 'selected' : ''}}>Pimpinan I</option>
                                        <option value="2" {{$data->role==2 ? 'selected' : ''}}>Pimpinan II</option>
                                        <option value="3" {{$data->role==3 ? 'selected' : ''}}>Verifikator</option>
                                        <option value="4" {{$data->role==4 ? 'selected' : ''}}>Operator</option>
                                        <option value="5" {{$data->role==5 ? 'selected' : ''}}>Admin</option>
                                    </select>
                                </div>
                                <div class="col-lg-3">
                                    <label>Sektor :</label>
                                    <select style="width:100%;" class="form-control" name="sektor" id="sektor">
                                        <option value="">Pilih Sektor</option>
                                        <option value="1" {{$data->sektor==1 ? 'selected' : ''}}>Air Minum</option>
                                        <option value="2" {{$data->sektor==2 ? 'selected' : ''}}>PKP</option>
                                        <option value="3" {{$data->sektor==3 ? 'selected' : ''}}>Sanitasi</option>
                                        <option value="4" {{$data->sektor==4 ? 'selected' : ''}}>BPB</option>
                                        <option value="5" {{$data->sektor==5 ? 'selected' : ''}}>Lintas Sektor</option>
                                    </select>
                                </div>
                                <div class="col-lg-3">
                                    <label>Status :</label>
                                    <select style="width:100%;" class="form-control" name="st" id="st">
                                        <option value="">Pilih Status</option>
                                        <option value="aktif" {{$data->st=="aktif" ? 'selected' : ''}}>Aktif</option>
                                        <option value="tidak aktif" {{$data->st=="tidak aktif" ? 'selected' : ''}}>Tidak Aktif</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-lg-6">
                                    @if ($data->id)
                                    <button id="tombol_simpan" type="button" onclick="handle_save('#tombol_simpan','#form_input','{{route('phln.user.update',$data->id)}}','PATCH');" class="btn btn-primary mr-2">Ubah</button>
                                    @else    
                                    <button id="tombol_simpan" type="button" onclick="handle_save('#tombol_simpan','#form_input','{{route('phln.user.store')}}','POST');" class="btn btn-primary mr-2">Simpan</button>
                                    @endif
                                </div>
                                @if ($data->id)
                                <div class="col-lg-6 text-lg-right">
                                    <button onclick="handle_confirm('Konfirmasi hapus data','Ya','Tidak','DELETE','{{route('phln.user.destroy',$data->id)}}');" type="button" class="btn btn-danger">Hapus</button>
                                </div>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    select2('role','Pilih Role');
    select2('sektor','Pilih Sektor');
</script>