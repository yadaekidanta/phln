<table class="table">
    <thead>
        <tr>
            <th>Username</th>
            <th>Nama</th>
            <th>Email</th>
            <th>Sektor</th>
            <th>Role</th>
            <th>Jabatan</th>
            <th>Status</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        @php
        $no = 1;
        @endphp
        @foreach ($collection as $item)
        @php
        $role = $item->role;
        $sektor = $item->sektor;
        if($role == 1){
            $role = 'Pimpinan I';
        }
        elseif($role == 2){
            $role = 'Pimpinan II';
        }
        elseif($role == 3){
            $role = 'Verifikator';
        }
        elseif($role == 4){
            $role = 'Operator';
        }
        elseif($role == 5){
            $role = 'Admin';
        }
        if($sektor == 1){
            $sektor = 'Air Minum';
        }
        elseif($sektor == 2){
            $sektor = 'PKP';
        }
        elseif($sektor == 3){
            $sektor = 'Sanitasi';
        }
        elseif($sektor == 4){
            $sektor = 'BPB';
        }
        elseif($sektor == 5){
            $sektor = 'Lintas Sektor';
        }
        @endphp
        <tr>
            <td>{{$item->username}}</td>
            <td>{{$item->nama}}</td>
            <td>{{$item->email}}</td>
            <td>{{$sektor}}</td>
            <td>{{$role}}</td>
            <td>{{$item->jabatan}}</td>
            <td>{{Str::title($item->st)}}</td>
            <td>
                <a href="javascript:;" onclick="load_input('{{route('phln.user.edit',$item->id)}}');" class="btn btn-icon btn-outline-warning btn-sm mr-2">
                    <i class="flaticon2-edit"></i>
                </a>
                <a href="javascript:;" onclick="handle_confirm('Konfirmasi hapus data','Ya','Tidak','DELETE','{{route('phln.user.destroy',$item->id)}}');" class="btn btn-icon btn-outline-danger btn-sm mr-2">
                    <i class="flaticon2-trash"></i>
                </a>
                @if($item->st == "tidak aktif")
                <a href="javascript:;" onclick="handle_confirm('Konfirmasi Aktifasi data','Ya','Tidak','PATCH','{{route('phln.user.aktif',$item->id)}}');" class="btn btn-icon btn-outline-success btn-sm mr-2">
                    <i class="flaticon2-checkmark"></i>
                </a>
                @else
                <a href="javascript:;" onclick="handle_confirm('Konfirmasi Tidak Aktif data','Ya','Tidak','PATCH','{{route('phln.user.taktif',$item->id)}}');" class="btn btn-icon btn-outline-info btn-sm mr-2">
                    <i class="flaticon-close"></i>
                </a>
                @endif
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{$collection->links('theme.app.pagination')}}