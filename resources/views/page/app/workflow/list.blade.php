<table class="table">
    <thead>
        <tr>
            <th>Urutan</th>
            <th>Nama</th>
            <th>Waktu(Hari)</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        @php
        $no = 1;
        @endphp
        @foreach ($collection as $item)
        <tr>
            <td>{{$item->urutan}}</td>
            <td>{{$item->nama}}</td>
            <td>{{$item->waktu}}</td>
            <td>
                <a href="javascript:;" onclick="load_input('{{route('phln.workflow.edit',$item->id)}}');" class="btn btn-icon btn-outline-warning btn-sm mr-2">
                    <i class="flaticon2-edit"></i>
                </a>
                <a href="javascript:;" onclick="handle_confirm('Konfirmasi hapus data','Ya','Tidak','DELETE','{{route('phln.workflow.destroy',$item->id)}}');" class="btn btn-icon btn-outline-danger btn-sm mr-2">
                    <i class="flaticon2-trash"></i>
                </a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{$collection->links('theme.app.pagination')}}