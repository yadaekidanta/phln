<div class="subheader py-3 py-lg-8 subheader-transparent">
    <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <div class="d-flex align-items-center mr-1">
            <div class="d-flex align-items-baseline flex-wrap mr-5">
                <h2 class="d-flex align-items-center text-dark font-weight-bold my-1 mr-3">Master Satker</h2>
                <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold my-2 p-0">
                    <li class="breadcrumb-item text-muted">
                        <a href="javascript:;" class="text-muted">Unit Kerja</a>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="javascript:;" class="text-muted">List Data</a>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="javascript:;" class="text-muted">
                            @if ($data->id)
                            Ubah
                            @else
                            Tambah
                            @endif
                            Data
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="d-flex align-items-center flex-wrap">
            <a href="javascript:;" onclick="load_list(1);" class="btn btn-primary btn-fixed-height font-weight-bold px-2 px-lg-5 mr-2" aria-haspopup="true" aria-expanded="false">
                <span class="d-none d-md-inline">Kembali</span>
            </a>
        </div>
    </div>
</div>
<div class="d-flex flex-column-fluid">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <form class="form" id="form_input">
                        <div class="card-body">
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label>Kode Satker:</label>
                                    <input value="{{$data->kode_satker}}" type="tel" id="kode_satker" name="kode_satker" maxlength="10" class="form-control" placeholder="Masukkan Kode Satker"/>
                                </div>
                                <div class="col-lg-6">
                                    <label>Nama Unit Kerja:</label>
                                    <input type="text" value="{{$data->nama}}" name="nama" class="form-control" placeholder="Masukkan Nama Unit Kerja"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-4">
                                    <label>Propinsi:</label>
                                    <select style="width:100%;" class="form-control select2" id="provinsi_id" name="provinsi_id">
                                        <option>Pilih Propinsi</option>
                                        @foreach ($province as $item)
                                            <option value="{{$item->id_prov}}">{{$item->nm_prov}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <label>Kota / Kabupaten:</label>
                                    <select style="width:100%;" class="form-control select2" name="kabupaten_id" id="kabupaten_id">
                                        <option>Harap pilih Propinsi terlebih dahulu</option>
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <label>Status:</label>
                                    <select style="width:100%;" class="form-control select2" name="st" id="st">
                                        <option value="">Pilih Status</option>
                                        <option value="aktif" {{$data->st == "aktif" ? 'selected' : ''}}>Aktif</option>
                                        <option value="tidak-aktif" {{$data->st == "tidak-aktif" ? 'selected' : ''}}>Tidak Aktif</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-lg-6">
                                    @if ($data->id)
                                    <button id="tombol_simpan" type="button" onclick="handle_save('#tombol_simpan','#form_input','{{route('phln.unit-kerja.update',$data->id)}}','PATCH');" class="btn btn-primary mr-2">Ubah</button>
                                    @else    
                                    <button id="tombol_simpan" type="button" onclick="handle_save('#tombol_simpan','#form_input','{{route('phln.unit-kerja.store')}}','POST');" class="btn btn-primary mr-2">Simpan</button>
                                    @endif
                                </div>
                                @if ($data->id)
                                <div class="col-lg-6 text-lg-right">
                                    <button onclick="handle_confirm('Konfirmasi hapus data','Ya','Tidak','DELETE','{{route('phln.unit-kerja.destroy',$data->id)}}');" type="button" class="btn btn-danger">Hapus</button>
                                </div>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    number_only('kode_satker')
    select2('provinsi_id','Pilih Provinsi');
    select2('kabupaten_id','Pilih Kota / Kabupaten');
    select2('st','Pilih Status');
    @if($data->provinsi_id)
    $('#provinsi_id').val('{{$data->provinsi_id}}');
    setTimeout(function(){ 
        $('#provinsi_id').trigger('change');
        setTimeout(function(){ 
            $('#kabupaten_id').val('{{$data->kabupaten_id}}');
            $('#kabupaten_id').trigger('change');
        }, 1500);
    }, 1000);
    @endif
    $("#provinsi_id").change(function(){
        $.ajax({
            type: "POST",
            url: "{{route('phln.get_city')}}",
            data: {id_prov : $("#provinsi_id").val()},
            success: function(response){
                $("#kabupaten_id").html(response);
            }
        });
    });
</script>