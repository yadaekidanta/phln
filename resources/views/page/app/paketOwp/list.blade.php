<table class="table table-bordered">
    <thead>
        <tr>
            <th class="text-center" rowspan='2'>No</th>
            <th class="text-center" rowspan='2'>Kode & Nama Paket</th>
            <th class="text-center" rowspan='2'>Alokasi</th>
            <th class="text-center" rowspan='2'>Realisasi Komulatif (T-1)</th>
            <th class="text-center" rowspan='2'>DIPA (Tahun Berjalan)</th>
            <th class="text-center" colspan='{{$tahun->count()}}'>Rencana Penyerapan (Tahun Berjalan)</th>
        </tr>
        <tr>
            @php
                $year_arr = array();
            @endphp
            @foreach ($tahun as $item)
                @if($item->ta > date("Y"))
                @php $year_arr[] = $item->ta; @endphp
                <th>{{$item->ta}}</th>
                @endif
            @endforeach
            
        </tr>
    </thead>
    <tbody>
        @php
        $no = 1;
        @endphp
        @foreach ($collection as $item)
        <tr>
            <td>{{$no++}}</td>
            <td>({{$item->kode_paket}}) - {{$item->nama_paket}}</td>
            <td>{{number_format($item->alokasi)}}</td>
            <td>{{number_format($realisasi)}}</td>
            <td>{{number_format($dipa)}}</td>
            @if($item->paketOwp->count() > 0)
                {{-- {{dd($item->paketOwp)}} --}}
                @foreach ($year_arr as $year)
                    @foreach ($item->paketOwp as $owp)
                        @php $val = 0;  @endphp
                        @if ($owp->ta == $year)
                            @php $val = $owp->target_dana;  @endphp
                            @break
                        @else
                        @endif
                    @endforeach
                <td>{{$val}}</td>
                @endforeach
            @else
            @endif
        </tr>
        @endforeach
    </tbody>
</table>
{{$collection->links('theme.app.pagination')}}