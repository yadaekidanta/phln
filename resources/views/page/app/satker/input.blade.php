<div class="subheader py-3 py-lg-8 subheader-transparent">
    <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <div class="d-flex align-items-center mr-1">
            <div class="d-flex align-items-baseline flex-wrap mr-5">
                <h2 class="d-flex align-items-center text-dark font-weight-bold my-1 mr-3">Master Satker</h2>
                <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold my-2 p-0">
                    <li class="breadcrumb-item text-muted">
                        <a href="javascript:;" class="text-muted">Satker</a>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="javascript:;" class="text-muted">List Data</a>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="javascript:;" class="text-muted">
                            @if ($data->id)
                            Ubah
                            @else
                            Tambah
                            @endif
                            Data
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="d-flex align-items-center flex-wrap">
            <a href="javascript:;" onclick="load_list(1);" class="btn btn-primary btn-fixed-height font-weight-bold px-2 px-lg-5 mr-2" aria-haspopup="true" aria-expanded="false">
                <span class="d-none d-md-inline">Kembali</span>
            </a>
        </div>
    </div>
</div>
<div class="d-flex flex-column-fluid">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <form class="form" id="form_input">
                        <div class="card-body">
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label>Kode :</label>
                                    <input value="{{$data->kode}}" type="tel" id="kode" name="kode" maxlength="10" class="form-control" placeholder="Masukkan Kode Satker"/>
                                </div>
                                <div class="col-lg-6">
                                    <label>Nama :</label>
                                    <input type="text" value="{{$data->nama}}" name="nama" id="nama" class="form-control" placeholder="Masukkan Nama Satker"/>
                                </div>
                                <div class="col-lg-6">
                                    <label>Department :</label>
                                    <input name="department_id" value="{{$unor->department->id}}" type="hidden">
                                    <input value="{{$unor->department->nama}}" type="text" readonly class="form-control">
                                </div>
                                <div class="col-lg-6">
                                    <label>Unor :</label>
                                    <input name="unor_id" value="{{$unor->id}}" type="hidden">
                                    <input value="{{$unor->nama}}" type="text" readonly class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-lg-6">
                                    @if ($data->id)
                                    <button id="tombol_simpan" type="button" onclick="handle_save('#tombol_simpan','#form_input','{{route('phln.satker.update',$data->id)}}','PATCH');" class="btn btn-primary mr-2">Ubah</button>
                                    @else    
                                    <button id="tombol_simpan" type="button" onclick="handle_save('#tombol_simpan','#form_input','{{route('phln.satker.store')}}','POST');" class="btn btn-primary mr-2">Simpan</button>
                                    @endif
                                </div>
                                @if ($data->id)
                                <div class="col-lg-6 text-lg-right">
                                    <button onclick="handle_confirm('Konfirmasi hapus data','Ya','Tidak','DELETE','{{route('phln.satker.destroy',$data->id)}}');" type="button" class="btn btn-danger">Hapus</button>
                                </div>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    number_only('kode')
    @if($data->department_id)
    $('#department_id').val('{{$data->department_id}}');
    setTimeout(function(){ 
        $('#department_id').trigger('change');
        setTimeout(function(){ 
            $('#unor_id').val('{{$data->unor_id}}');
            $('#unor_id').trigger('change');
        }, 1500);
    }, 1000);
    @endif
    $("#department_id").change(function(){
        $.ajax({
            type: "POST",
            url: "{{route('phln.get_unor')}}",
            data: {department_id : $("#department_id").val()},
            success: function(response){
                $("#unor_id").html(response);
            }
        });
    });
</script>