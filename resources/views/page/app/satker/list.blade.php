<table class="table">
    <thead>
        <tr>
            <th>ID</th>
            <th>Kode Satker</th>
            <th>Nama Satker</th>
            <th>Department</th>
            <th>Unor</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        @php
        $no = 1;
        @endphp
        @foreach ($collection as $item)
        <tr>
            <td>{{$no++}}</td>
            <td>{{$item->kode}}</td>
            <td>{{$item->nama}}</td>
            <td>{{$item->department->nama}}</td>
            <td>{{$item->unor->nama}}</td>
            <td>
                <a href="javascript:;" onclick="load_input('{{route('phln.satker.edit',[$item->unor->id,$item->id])}}');" class="btn btn-icon btn-outline-warning btn-sm mr-2">
                    <i class="flaticon2-edit"></i>
                </a>
                <a href="javascript:;" onclick="handle_confirm('Konfirmasi hapus data','Ya','Tidak','DELETE','{{route('phln.satker.destroy',$item->id)}}');" class="btn btn-icon btn-outline-danger btn-sm mr-2">
                    <i class="flaticon2-trash"></i>
                </a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{$collection->links('theme.app.pagination')}}