<x-web-layout title="Home">
    <section id="content">
        <div class="content-wrap">
            <div class="container clearfix">
                <h3>
                    Tentang {{config('app.name')}}
                </h3>
                <p><span class="dropcap">D</span>alam rangka mencapai sasaran program pembangunan Bidang Cipta Karya yang telah dituangkan dalam Rencana Pembangunan Jangka Menengah Nasional (RPJMN) Tahun 2015-2019 dan Rencana Strategis (Renstra)Bidang Cipta Karya Tahun 2015-2019, Pinjaman dan Hibah Luar Negeri merupakan salah satu alternatif sumber pembiayaan yang diperlukan untuk mempercepat pencapaian sasaran pembangunan nasional, di samping sumber pembiayaan dari Anggaran Pendapatan dan Belanja Negara (APBN) dan Anggaran Pendapatan dan Belanja Daerah (APBD).</p>
                <p>Saat ini Pemerintah dihadapkan kepada kebutuhan investasi untuk membangun ekonomi dalam rangka memperbaiki tingkat kesejahteraan rakyat dan memenuhi kebutuhan dasar, menurunkan angka kemiskinan serta membangun dan memperbaiki infrastruktur Bidang Cipta Karya. Selain itu, Pemerintah juga membutuhkan investasi yang besar untuk katalisator dan dinamisator pembangunan yang dihadapkan dalam keterbatasan sumber dana Pemerintah, baik APBN maupun APBD</p>
                <p>Pengelolaan Pinjaman dan Hibah Luar Negeri (PHLN) menganut prinsip-prinsip Pemerintahan yang baik dan juga mengikuti Standar Akuntansi Pemerintahan sesuai dengan peraturan perundang-undangan yang berlaku. Dengan sistem informasi ini diharapkan dapat tercapai efektivitas dan efisiensi proses pemantauan kegiatan di lingkungan Direktorat Jenderal Cipta Karya yang pembiayaannya berasal dari Pinjaman dan Hibah Luar Negeri.</p>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="heading-block fancy-title border-bottom-0 title-bottom-border">
                            <h4><span>Maksud</span>.</h4>
                        </div>
                        <p>Kegiatan ini dimaksudkan sebagai alat bagi para pengelola kegiatan dalam melaksanakan pemantauan semua ketentuan yang berhubungan dengan administrasi pengelolaan pinjaman dan hibah luar negeri pada unit/satuan kerja lingkup Ditjen Cipta Karya, agar pengelolaannya dapat dilakukan dengan akuntabel</p>
                    </div>
                    <div class="col-lg-6">
                        <div class="heading-block fancy-title border-bottom-0 title-bottom-border">
                            <h4><span>Tujuan</span>.</h4>
                        </div>
                        <p>Tujuan pekerjaan ini adalah untuk mempermudah dan menyeragamkan pemantauan pelaksanaan pengelolaan pinjaman dan hibah luar negeri sehingga terwujud tertib administrasi dan tertib anggaran dalam rangka menunjang visi dan misi Ditjen Cipta Karya</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</x-web-layout>