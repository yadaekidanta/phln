<div id="kt_header_mobile" class="header-mobile">
    <!--begin::Logo-->
    <a href="javascript:;">
        <img alt="Logo" src="{{asset('img/icon.jpg')}}" class="logo-default max-h-30px" />
    </a>
    <!--end::Logo-->
    <!--begin::Toolbar-->
    <div class="d-flex align-items-center">
        <button class="btn p-0 burger-icon burger-icon-left" id="kt_aside_mobile_toggle">
            <span></span>
        </button>
    </div>
    <!--end::Toolbar-->
</div>