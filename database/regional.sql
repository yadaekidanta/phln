/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : PostgreSQL
 Source Server Version : 120007
 Source Host           : localhost:5432
 Source Catalog        : phln
 Source Schema         : regional

 Target Server Type    : PostgreSQL
 Target Server Version : 120007
 File Encoding         : 65001

 Date: 06/09/2021 20:03:54
*/


-- ----------------------------
-- Sequence structure for provinsi_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "regional"."provinsi_id_seq";
CREATE SEQUENCE "regional"."provinsi_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Table structure for kabupaten
-- ----------------------------
DROP TABLE IF EXISTS "regional"."kabupaten";
CREATE TABLE "regional"."kabupaten" (
  "id" int8 NOT NULL GENERATED ALWAYS AS IDENTITY (
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
),
  "id_prov" varchar(2) COLLATE "pg_catalog"."default",
  "id_kab" varchar(2) COLLATE "pg_catalog"."default",
  "nm_kab" varchar(100) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for provinsi
-- ----------------------------
DROP TABLE IF EXISTS "regional"."provinsi";
CREATE TABLE "regional"."provinsi" (
  "id" int4 NOT NULL GENERATED ALWAYS AS IDENTITY (
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
),
  "id_prov" varchar(2) COLLATE "pg_catalog"."default",
  "nm_prov" varchar(100) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "regional"."kabupaten_id_seq"
OWNED BY "regional"."kabupaten"."id";
SELECT setval('"regional"."kabupaten_id_seq"', 515, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "regional"."provinsi_id_seq"
OWNED BY "regional"."provinsi"."id";
SELECT setval('"regional"."provinsi_id_seq"', 35, true);

-- ----------------------------
-- Primary Key structure for table kabupaten
-- ----------------------------
ALTER TABLE "regional"."kabupaten" ADD CONSTRAINT "kabupaten_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table provinsi
-- ----------------------------
ALTER TABLE "regional"."provinsi" ADD CONSTRAINT "provinsi_pkey" PRIMARY KEY ("id");
