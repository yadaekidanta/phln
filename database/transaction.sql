/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : PostgreSQL
 Source Server Version : 120007
 Source Host           : localhost:5432
 Source Catalog        : phln
 Source Schema         : transaction

 Target Server Type    : PostgreSQL
 Target Server Version : 120007
 File Encoding         : 65001

 Date: 27/09/2021 21:34:53
*/


-- ----------------------------
-- Sequence structure for adendum_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "transaction"."adendum_id_seq";
CREATE SEQUENCE "transaction"."adendum_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for kegiatan_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "transaction"."kegiatan_id_seq";
CREATE SEQUENCE "transaction"."kegiatan_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for paket_awp_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "transaction"."paket_awp_id_seq";
CREATE SEQUENCE "transaction"."paket_awp_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for paket_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "transaction"."paket_id_seq";
CREATE SEQUENCE "transaction"."paket_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for paket_owp_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "transaction"."paket_owp_id_seq";
CREATE SEQUENCE "transaction"."paket_owp_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Table structure for adendum
-- ----------------------------
DROP TABLE IF EXISTS "transaction"."adendum";
CREATE TABLE "transaction"."adendum" (
  "id" int8 NOT NULL GENERATED ALWAYS AS IDENTITY (
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
),
  "kegiatan_id" int8,
  "paket_id" int8,
  "nilai_kontrak" float8,
  "tanggal_akhir_kontrak" date,
  "alasan" text COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for kegiatan
-- ----------------------------
DROP TABLE IF EXISTS "transaction"."kegiatan";
CREATE TABLE "transaction"."kegiatan" (
  "id" int8 NOT NULL GENERATED ALWAYS AS IDENTITY (
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
),
  "kode_register" varchar(255) COLLATE "pg_catalog"."default",
  "no_loan" varchar(255) COLLATE "pg_catalog"."default",
  "donor_id" int4,
  "mata_uang_id" int4,
  "judul" varchar(255) COLLATE "pg_catalog"."default",
  "tujuan" text COLLATE "pg_catalog"."default",
  "nilai" float8,
  "tanggal_efektif" date,
  "tanggal_closing" date,
  "exec_kl_id" int4,
  "exec_unor_id" int4,
  "exec_satker_id" int4,
  "imp_kl_id" int4,
  "imp_unor_id" int4,
  "imp_satker_id" int4,
  "dipa_pagu" float8,
  "dipa_anggaran" float8,
  "kinerja_serapan" float8,
  "jumlah_penarikan" float8,
  "ta" varchar(4) COLLATE "pg_catalog"."default",
  "tipe_kegiatan" varchar(15) COLLATE "pg_catalog"."default",
  "etr" float8,
  "dr" float8,
  "pv" float8,
  "penyerapan" float8,
  "st" varchar(30) COLLATE "pg_catalog"."default",
  "nilai_konversi" float8,
  "sasaran" text COLLATE "pg_catalog"."default",
  "komponen" text COLLATE "pg_catalog"."default",
  "lingkup_kegiatan" text COLLATE "pg_catalog"."default",
  "sektor" int2,
  "metode_pembayaran" int2
)
;
COMMENT ON COLUMN "transaction"."kegiatan"."sektor" IS '1 = Air Minum
2 = PKP
3 = Sanitasi
4 = BPB
5 = Lintas Sektor';
COMMENT ON COLUMN "transaction"."kegiatan"."metode_pembayaran" IS '1 = PP
2 = PL
3 = LC
4 = RK';

-- ----------------------------
-- Table structure for paket
-- ----------------------------
DROP TABLE IF EXISTS "transaction"."paket";
CREATE TABLE "transaction"."paket" (
  "id" int8 NOT NULL GENERATED ALWAYS AS IDENTITY (
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
),
  "kegiatan_id" int8,
  "prov_id" int4,
  "kab_id" int4,
  "penarikan_id" int8,
  "kode_paket" varchar(255) COLLATE "pg_catalog"."default",
  "nama_paket" varchar(255) COLLATE "pg_catalog"."default",
  "alokasi" float8,
  "nilai_kontrak" float8,
  "tanggal_mkontrak" date,
  "tanggal_skontrak" date,
  "tanggal_mtender" date,
  "tanggal_stender" date,
  "st_tender" int2,
  "penyedia_jasa" varchar(255) COLLATE "pg_catalog"."default",
  "jenis_paket" int2
)
;
COMMENT ON COLUMN "transaction"."paket"."st_tender" IS '0 = Belum
1 = Proses
2 = Kontrak';
COMMENT ON COLUMN "transaction"."paket"."jenis_paket" IS '1 = Kontraktual
2 = Swakelola
3 = Administrasi Umum';

-- ----------------------------
-- Table structure for paket_awp
-- ----------------------------
DROP TABLE IF EXISTS "transaction"."paket_awp";
CREATE TABLE "transaction"."paket_awp" (
  "id" int8 NOT NULL GENERATED ALWAYS AS IDENTITY (
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
),
  "kegiatan_id" int8,
  "paket_id" int8,
  "ta" varchar(4) COLLATE "pg_catalog"."default",
  "quartal" int2,
  "target_dana" float8,
  "real_dana" float8,
  "target_fisik" float8,
  "real_fisik" float8,
  "masalah" text COLLATE "pg_catalog"."default",
  "tindak_lanjut" text COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "transaction"."paket_awp"."quartal" IS '1 = Q1
2 = Q2
3 = Q3
4 = Q4';

-- ----------------------------
-- Table structure for paket_owp
-- ----------------------------
DROP TABLE IF EXISTS "transaction"."paket_owp";
CREATE TABLE "transaction"."paket_owp" (
  "id" int8 NOT NULL GENERATED ALWAYS AS IDENTITY (
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
),
  "paket_id" int8,
  "tahun" varchar(4) COLLATE "pg_catalog"."default",
  "nilai" float8
)
;

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "transaction"."adendum_id_seq"
OWNED BY "transaction"."adendum"."id";
SELECT setval('"transaction"."adendum_id_seq"', 2, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "transaction"."kegiatan_id_seq"
OWNED BY "transaction"."kegiatan"."id";
SELECT setval('"transaction"."kegiatan_id_seq"', 6, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "transaction"."paket_awp_id_seq"
OWNED BY "transaction"."paket_awp"."id";
SELECT setval('"transaction"."paket_awp_id_seq"', 4, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "transaction"."paket_id_seq"
OWNED BY "transaction"."paket"."id";
SELECT setval('"transaction"."paket_id_seq"', 4, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "transaction"."paket_owp_id_seq"
OWNED BY "transaction"."paket_owp"."id";
SELECT setval('"transaction"."paket_owp_id_seq"', 2, false);
