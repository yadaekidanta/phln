/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : PostgreSQL
 Source Server Version : 120007
 Source Host           : localhost:5432
 Source Catalog        : phln
 Source Schema         : hrm

 Target Server Type    : PostgreSQL
 Target Server Version : 120007
 File Encoding         : 65001

 Date: 05/09/2021 15:17:21
*/


-- ----------------------------
-- Sequence structure for users_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "hrm"."users_id_seq";
CREATE SEQUENCE "hrm"."users_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS "hrm"."users";
CREATE TABLE "hrm"."users" (
  "id" int8 NOT NULL DEFAULT nextval('"hrm".users_id_seq'::regclass),
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "email" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "email_verified_at" timestamp(0),
  "password" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "remember_token" varchar(100) COLLATE "pg_catalog"."default",
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "hrm"."users_id_seq"
OWNED BY "hrm"."users"."id";
SELECT setval('"hrm"."users_id_seq"', 3, false);

-- ----------------------------
-- Uniques structure for table users
-- ----------------------------
ALTER TABLE "hrm"."users" ADD CONSTRAINT "hrm_users_email_unique" UNIQUE ("email");

-- ----------------------------
-- Primary Key structure for table users
-- ----------------------------
ALTER TABLE "hrm"."users" ADD CONSTRAINT "users_pkey" PRIMARY KEY ("id");
